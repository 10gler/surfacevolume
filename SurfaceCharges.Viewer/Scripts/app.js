﻿/// <reference path="app_three.js" />
/// <reference path="underscore.js" />
/// <reference path="jstat-1.0.0.js" />
/// <reference path="knockout-2.2.1.debug.js" />

'use strict';

var ViewModel;

function makeField(field) {
    return {
        min: _.min(field),
        max: _.max(field),
        values: field
    };
}

function makeDiffField(fieldA, fieldB) {
    var data = [];

    for (var i = 0; i < fieldA.length; i++) {
        data.push(fieldA[i] - fieldB[i]);
    }

    return {
        min: _.min(data),
        max: _.max(data),
        values: data
    };
}

function ViewModel(result) {

    var self = this;

    this.result = result;
    this.surface = result.Surface;
    this.fieldNames = _.keys(result.Fields);
    this.fields = {};
    _.each(this.fieldNames, function (f) { self.fields[f] = makeField(result.Fields[f]); });

    this.currentField = ko.observable(this.fieldNames[0]);

    this.diffFieldA = ko.observable(this.fieldNames[0]);
    this.diffFieldB = ko.observable(this.fieldNames[1]);
    this.fieldStats = ko.observable({ name: 'none', min: 0.0, max: 0.0 });

    this.applyCurrentField = function () {
        var field = self.fields[self.currentField()];
        self.fieldStats({ name: self.currentField(), min: field.min, max: field.max });

        self.result.Tunnels.forEach(function (t) {
            createMesh(t.Surface, makeField(t.Fields[self.currentField()]))
        });
        createTransparentSurface(self.surface, self.fields[self.currentField()]);
        //createSurface(self.surface, self.fields[self.currentField()]);
    };

    this.applyDiffField = function () {
        var field = makeDiffField(self.result.Fields[self.diffFieldA()], self.result.Fields[self.diffFieldB()]);
        self.fieldStats({ name: self.diffFieldA() + ' - ' + self.diffFieldB(), min: field.min, max: field.max });
        createSurface(self.surface, field);
    };


    this.analyzeFieldA = ko.observable(this.fieldNames[0]);
    this.analyzeFieldB = ko.observable(this.fieldNames[1]);
    this.fieldAnalysis = ko.observable();
    this.chargeAnalysis = ko.observable();


    this.showAnalysis = function () {
        var fA = self.analyzeFieldA(), fB = self.analyzeFieldB();
        if (fA === fB) return;

        self.chargeAnalysis(JSON.stringify(self.result.ChargeCorrelations[fA][fB], undefined, "  "));
        self.fieldAnalysis(JSON.stringify(self.result.FieldCorrelations[fA][fB], undefined, "  "));
    };

    this.analyzeFieldA.subscribe(this.showAnalysis);
    this.analyzeFieldB.subscribe(this.showAnalysis);
    this.showAnalysis();


    this.resetCamera = function () {
        resetSceneCamera();
    };

    this.applyCurrentField();
}

function handleFileSelect(evt) {
    var file = evt.target.files[0]; // FileList object

    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function (theFile) {
        return function (e) {
            var result = JSON.parse(e.target.result);
            $('#filename').text(file.name);
            ko.applyBindings(new ViewModel(result));
        };
    })(file);

    // Read in the image file as a data URL.
    reader.readAsText(file);
}

function drawTest() {
    //var geometry = new THREE.Geometry();
    //geometry.vertices.push(new THREE.Vector3(-10, 10, 0));
    //geometry.vertices.push(new THREE.Vector3(-10, -10, 0));
    //geometry.vertices.push(new THREE.Vector3(10, -10, 0));

    //geometry.faces.push(new THREE.Face3(0, 1, 2));

    var radius = 50,
    segments = 16,
    rings = 16;

    var sphereMaterial =
        new THREE.MeshLambertMaterial(
        {
            color: 0xCC0000
        });

    var sphere = new THREE.Mesh(new THREE.SphereGeometry(
        radius,
        segments,
        rings), sphereMaterial);

    scene.add(sphere);
}

$(function () {
    init3d();
    if (Detector.webgl) {
        animate();
    }

    drawTest();

    $("#files").on('change', handleFileSelect);

    $('a[data-toggle="tab"]').on('shown', function (e) {
        if ($(e.target).attr('href') === "#visualization") {
            updateScene()
        }
    });
});