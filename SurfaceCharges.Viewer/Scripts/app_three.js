﻿/// <reference path="threejs/three.js" />
/// <reference path="underscore.js" />

var container, scene, camera, renderer, controls, stats;
var clock = new THREE.Clock();

var surfaceGeometry;
var gridMaterial, wireMaterial, vertexColorMaterial;
var surfaceMesh;

function handleResize() {
    $(window).off('resize');

    (function (renderer, camera, $scene) {
        $(window).resize(function (evt) {
            var width = $scene.width(), height = $scene.height();

            renderer.setSize(width, height);
            // update the camera
            camera.aspect = width / height;
            camera.updateProjectionMatrix();
        });
    })(renderer, camera, $('#scene'));
}

function updateScene() {
    (function (renderer, camera, $scene) {
        var width = $scene.width(), height = $scene.height();

        renderer.setSize(width, height);
        // update the camera
        camera.aspect = width / height;
        camera.updateProjectionMatrix();
    })(renderer, camera, $('#scene'));
}

// FUNCTIONS 		
function init3d() {
    // SCENE
    scene = new THREE.Scene();

    var $scene = $('#scene');

    var width = $scene.width(), height = $scene.height();


    // CAMERA
    var SCREEN_WIDTH = width, SCREEN_HEIGHT = height;
    var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
    camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
    scene.add(camera);
    //new THREE.Vector3();
    var posTmp = new THREE.Vector3(22809.587, -24624.598, 30);
    camera.position.set(0, 0, 150);
    //camera.position = posTmp;
    camera.lookAt(scene.position);
    //camera.lookAt(posTmp);
    // RENDERER
    if (Detector.webgl)
        renderer = new THREE.WebGLRenderer({ antialias: true });
    else {
        //$("<span style='color: white'>WebGL not available</span>").get(0)
        $scene.html("<span style='color: white'>WebGL not available</span>");
        return;
    }
    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    container = document.getElementById('scene');
    container.appendChild(renderer.domElement);
    // EVENTS

    handleResize();

    // CONTROLS
    controls = new THREE.TrackballControls(camera, renderer.domElement);
    // STATS
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.bottom = '0px';
    stats.domElement.style.zIndex = 100;
    container.appendChild(stats.domElement);
    // LIGHT
    var light = new THREE.PointLight(0xffffff);
    light.position.set(0, 250, 0);
    scene.add(light);

    ////light = new THREE.PointLight(0xffffff);
    ////light.position.set(0, -250, 0);
    ////scene.add(light);

    ////light = new THREE.PointLight(0xffffff);
    ////light.position.set(250, 0, 0);
    ////scene.add(light);

    ////light = new THREE.PointLight(0xffffff);
    ////light.position.set(-250, 0, 0);
    ////scene.add(light);

    ////light = new THREE.PointLight(0xffffff);
    ////light.position.set(0, 0, 250);
    ////scene.add(light);

    ////light = new THREE.PointLight(0xffffff);
    ////light.position.set(0, 0, -250);
    ////scene.add(light);

    ////var light2 = new THREE.AmbientLight(0x333333);
    ////light2.position.set(light.position);
    ////scene.add(light2);
    // SKYBOX/FOG
    // scene.fog = new THREE.FogExp2( 0x888888, 0.00025 );

    ////////////
    // CUSTOM //
    ////////////

    scene.add(new THREE.AxisHelper());

    ////// wireframe for xy-plane
    ////var wireframeMaterial = new THREE.MeshBasicMaterial({ color: 0x000088, wireframe: true, side: THREE.DoubleSide });
    ////var floorGeometry = new THREE.PlaneGeometry(1000, 1000, 10, 10);
    ////var floor = new THREE.Mesh(floorGeometry, wireframeMaterial);
    ////floor.position.z = -0.01;
    ////// rotate to lie in x-y plane
    ////// floor.rotation.x = Math.PI / 2;
    ////scene.add(floor);

    ////var normMaterial = new THREE.MeshNormalMaterial;
    ////var shadeMaterial = new THREE.MeshLambertMaterial({ color: 0xff0000 });

    ////// "wireframe texture"
    ////var wireTexture = new THREE.ImageUtils.loadTexture('../Content/images/square.png');
    ////wireTexture.wrapS = wireTexture.wrapT = THREE.RepeatWrapping;
    ////wireTexture.repeat.set(40, 40);
    ////wireMaterial = new THREE.MeshBasicMaterial({ map: wireTexture, vertexColors: THREE.VertexColors, side: THREE.DoubleSide });

    var vertexColorMaterial = new THREE.MeshBasicMaterial({ vertexColors: THREE.VertexColors });

    // bgcolor
    renderer.setClearColor(new THREE.Color(0));
}

var minColor = new THREE.Color(0x2B5D92),
    maxColor = new THREE.Color(0xE30016);

var dMinR = (255.0 - minColor.r) ,
    dMinG = (255.0 - minColor.g) ,
    dMinB = (255.0 - minColor.b) ,

    dMaxR = (255.0 - maxColor.r) ,
    dMaxG = (255.0 - maxColor.g) ,
    dMaxB = (255.0 - maxColor.g) ;

var counter = 0;

function getColor(min, max, value) {
    if (value < 0) {
        var t = (255 * value / min) | 0;
        return new THREE.Color((0xff << 16) | ((0xff - t) << 8) | (0xff - t));
    } else {
        var t = (255 * value / max) | 0;
        return new THREE.Color(0xff | ((0xff - t) << 16) | ((0xff - t) << 8));
    }
}

////function changeColors() {
////    surfaceGeometry.faces.forEach(function (f) {
////        f.vertexColors = [new THREE.Color(0xff0000), new THREE.Color(0xff00), new THREE.Color(0xff)];
////    });
////    scene.remove(surfaceMesh);

////    surfaceMesh = new THREE.Mesh(surfaceGeometry, new THREE.MeshLambertMaterial({ color: 0xffffff, vertexColors: THREE.VertexColors, side: THREE.DoubleSide, opacity: 0.7 }));
////    scene.add(surfaceMesh);
////}
///

function createMesh(surface, field) {

    if (!Detector.webgl) {
        return;
    }

    var geometry = new THREE.Geometry();

    surface.Vertices.forEach(function (v) { geometry.vertices.push(new THREE.Vector3(v[0], v[1], v[2])); });
    surface.Triangles.forEach(function (t) {
        geometry.faces.push(new THREE.Face3(t[0], t[1], t[2], null, [
            getColor(field.min, field.max, field.values[t[0]]),
            getColor(field.min, field.max, field.values[t[1]]),
            getColor(field.min, field.max, field.values[t[2]])
        ]));

        //geom.faces.push(new THREE.Face3(t[0], t[1], t[2]));
    });

    var mesh = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: 0xffffff, vertexColors: THREE.VertexColors, side: THREE.DoubleSide }));
    //surfaceMesh.geometry
    scene.add(mesh);
}

function createTransparentSurface(surface, field) {

    if (!Detector.webgl) {
        return;
    }

    var geom = new THREE.Geometry();

    surface.Vertices.forEach(function (v) { geom.vertices.push(new THREE.Vector3(v[0], v[1], v[2])); });
    surface.Triangles.forEach(function (t) {
        geom.faces.push(new THREE.Face3(t[0], t[1], t[2], null, [
            getColor(field.min, field.max, field.values[t[0]]),
            getColor(field.min, field.max, field.values[t[1]]),
            getColor(field.min, field.max, field.values[t[2]])
        ]));

        //geom.faces.push(new THREE.Face3(t[0], t[1], t[2]));
    });

    var mesh = new THREE.Mesh(geom, new THREE.MeshBasicMaterial({ color: 0x999999, vertexColors: THREE.VertexColors, side: THREE.DoubleSide, transparent: true, opacity: 0.25 }));
    //surfaceMesh.geometry
    scene.add(mesh);
}

function createWireSurface(surface, field) {

    if (!Detector.webgl) {
        return;
    }

    var geom = new THREE.Geometry();

    surface.Vertices.forEach(function (v) { geom.vertices.push(new THREE.Vector3(v[0], v[1], v[2])); });
    surface.Triangles.forEach(function (t) {
        geom.faces.push(new THREE.Face3(t[0], t[1], t[2], null, [
            getColor(field.min, field.max, field.values[t[0]]),
            getColor(field.min, field.max, field.values[t[1]]),
            getColor(field.min, field.max, field.values[t[2]])
        ]));

        //geom.faces.push(new THREE.Face3(t[0], t[1], t[2]));
    });

    var mesh = new THREE.Mesh(geom, new THREE.MeshBasicMaterial({ color: 0x999999, vertexColors: THREE.VertexColors, side: THREE.DoubleSide, wireframe: true }));
    //surfaceMesh.geometry
    scene.add(mesh);
}

function createSurface(data, field) {

    if (!Detector.webgl) {
        return;
    }

    if (!!surfaceMesh) {
        scene.remove(surfaceMesh);
        sufaceMesh = null;
    }
    
    surfaceGeometry = new THREE.Geometry();

    var geom = surfaceGeometry;
    
    data.Vertices.forEach(function (v) { geom.vertices.push(new THREE.Vector3(v[0], v[1], v[2])); });
    data.Triangles.forEach(function (t) {
        geom.faces.push(new THREE.Face3(t[0], t[1], t[2], null, [
            getColor(field.min, field.max, field.values[t[0]]),
            getColor(field.min, field.max, field.values[t[1]]),
            getColor(field.min, field.max, field.values[t[2]])
        ]));

        //geom.faces.push(new THREE.Face3(t[0], t[1], t[2]));
    });
    
    surfaceMesh = new THREE.Mesh(geom, new THREE.MeshBasicMaterial({ color: 0xffffff, vertexColors: THREE.VertexColors, side: THREE.DoubleSide }));
    //surfaceMesh.geometry
    scene.add(surfaceMesh);
    

    ////xRange = xMax - xMin;
    ////yRange = yMax - yMin;
    ////zFunc = Parser.parse(zFuncText).toJSFunction(['x', 'y']);
    ////meshFunction = function (x, y) {
    ////    x = xRange * x + xMin;
    ////    y = yRange * y + yMin;
    ////    var z = zFunc(x, y); //= Math.cos(x) * Math.sqrt(y);
    ////    if (isNaN(z))
    ////        return new THREE.Vector3(0, 0, 0); // TODO: better fix
    ////    else
    ////        return new THREE.Vector3(x, y, z);
    ////};

    ////// true => sensible image tile repeat...
    ////graphGeometry = new THREE.ParametricGeometry(meshFunction, segments, segments, true);

    ///////////////////////////////////////////////////
    ////// calculate vertex colors based on Z values //
    ///////////////////////////////////////////////////
    ////graphGeometry.computeBoundingBox();
    ////zMin = graphGeometry.boundingBox.min.z;
    ////zMax = graphGeometry.boundingBox.max.z;
    ////zRange = zMax - zMin;
    ////var color, point, face, numberOfSides, vertexIndex;
    ////// faces are indexed using characters
    ////var faceIndices = ['a', 'b', 'c', 'd'];
    ////// first, assign colors to vertices as desired
    ////for (var i = 0; i < graphGeometry.vertices.length; i++) {
    ////    point = graphGeometry.vertices[i];
    ////    color = new THREE.Color(0x0000ff);
    ////    color.setHSL(0.7 * (zMax - point.z) / zRange, 1, 0.5);
    ////    graphGeometry.colors[i] = color; // use this array for convenience
    ////}
    ////// copy the colors as necessary to the face's vertexColors array.
    ////for (var i = 0; i < graphGeometry.faces.length; i++) {
    ////    face = graphGeometry.faces[i];
    ////    numberOfSides = (face instanceof THREE.Face3) ? 3 : 4;
    ////    for (var j = 0; j < numberOfSides; j++) {
    ////        vertexIndex = face[faceIndices[j]];
    ////        face.vertexColors[j] = graphGeometry.colors[vertexIndex];
    ////    }
    ////}
    ///////////////////////////
    ////// end vertex colors //
    ///////////////////////////

    ////// material choices: vertexColorMaterial, wireMaterial , normMaterial , shadeMaterial

    ////if (graphMesh) {
    ////    scene.remove(graphMesh);
    ////    // renderer.deallocateObject( graphMesh );
    ////}

    ////wireMaterial.map.repeat.set(segments, segments);

    ////graphMesh = new THREE.Mesh(graphGeometry, wireMaterial);
    ////graphMesh.doubleSided = true;
    ////scene.add(graphMesh);
}

function resetSceneCamera() {

    //camera.position.set(0, 0, 150);
    //camera.rotation.set(0, 0, 0);
    //camera.lookAt(new THREE.Vector3(0, 0, 0));
   
    //camera.up.set(0, 0, 1);
    //camera.lookAt(scene.position);

    ////// CAMERA
    ////var $scene = $('#scene');
    ////var width = $scene.width(), height = $scene.height();

    ////// CAMERA
    ////var SCREEN_WIDTH = width, SCREEN_HEIGHT = height;
    ////var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
    //camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
    ////camera.position.set(0, 0, 150);
    ////camera.up = new THREE.Vector3(0, 0, 1);
    ////camera.lookAt(scene.position);
    ////scene.remove(camera);
    ////scene.add(camera);

    ////controls = new THREE.TrackballControls(camera, renderer.domElement);
    ////handleResize();
}

function animate() {
    requestAnimationFrame(animate);
    render();
    update();
}

function update() {
    controls.update();
    stats.update();
}

function render() {
    renderer.render(scene, camera);
}
