﻿using SurfaceVolume2.Math;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SurfaceVolume2
{
    static class VolumeDebug
    {
        static StringBuilder messages = new StringBuilder();

        public static void Message(string format, params object[] args)
        {
            messages.AppendLine(string.Format(format, args));
        }

        public static void Dump(string fn)
        {
            System.IO.File.WriteAllText(fn, messages.ToString());
        }

        public static string ToMathematicaPolygon(Polygon p, string name)
        {
            return string.Format("{0} = Polygon[{{{1}}}];", name, string.Join(", ", p.Vertices.Select(v => string.Format("{{{0},{1}}}", v.X, v.Y))));
        }

        public static string ToMathematicaPolygon(IEnumerable<Vector2D> vert, string name)
        {
            return string.Format("{0} = Polygon[{{{1}}}];", name, string.Join(", ", vert.Select(v => string.Format("{{{0},{1}}}", v.X, v.Y))));
        }

        public static string ToMathematicaPolygons(params IEnumerable<Vector2D>[] vert)
        {
            var colors = new[] { "Red", "Green", "Blue", "Pink", "Brown", "Yellow" };

            var ps = String.Concat(Enumerable.Range(0, vert.Length)
                .Select(i => string.Format("vs[{0}] = Polygon[{{{1}}}];", i, string.Join(", ", vert[i].Select(v => string.Format("{{{0},{1}}}", v.X, v.Y)))))
                .ToArray());
            var g = string.Join(",", Enumerable.Range(0, vert.Length).Select(i => string.Format("{{{0}, vs[{1}]}}", colors[i], i)));

            return string.Format("{0} Graphics[{{{1}}}]", ps, g);
        }

        public static string ToMathematicaPolygon3D(IEnumerable<Vertex3D> vert, string name)
        {
            return string.Format("{0} = Polygon[{{{1}}}];", name, string.Join(", ", vert.Select(v => string.Format("{{{0},{1},{2}}}", v.Position.X, v.Position.Y, v.Position.Z))));
        }

        static List<Tuple<bool, Vector2D[]>> triangles = new List<Tuple<bool, Vector2D[]>>();

        public static void AddTriangle(double volume, params Vector2D[] vs)
        {
            triangles.Add(Tuple.Create(volume > -0.00001, vs));
        }

        static void WriteMathematicaTriangles(List<Tuple<bool, Vector2D[]>> triangles, string filename)
        {
            var ps = "{" + String.Join(",\n", Enumerable.Range(0, triangles.Count)
                .Select(i => "{" + (triangles[i].Item1 ? "Pink,{" : "Brown,{") + string.Join(",", triangles[i].Item2.Select(v => string.Format("{{{0},{1}}}", v.X, v.Y))) + "}}")
                .ToArray()) + "}";
            //var g = string.Join(",", Enumerable.Range(0, triangles.Count).Select(i => string.Format("{{{0}, {1}, Polygon[vs[[{2}]]}]}", "EdgeForm[Dashed]", triangles[i].Item1 ? "Pink" : "Brown", i)));
            var table = string.Format("Table[{{EdgeForm[Dashed],vs[[i,1]],Polygon[vs[[i,2]]]}}, {{i, 1, {0}}}]", triangles.Count);
            File.WriteAllText("i:/test/SurfaceVolume/" + filename, string.Format("vs = {0}; polys = {1}; Graphics[polys]", ps, table));
        }

        public static void WriteMathematicaTriangles()
        {
            WriteMathematicaTriangles(triangles, "all.txt");
            WriteMathematicaTriangles(triangles.Where(t => t.Item1).ToList(), "all_plus.txt");
            WriteMathematicaTriangles(triangles.Where(t => !t.Item1).ToList(), "all_minus.txt");
        }
    }

    class Tests
    {
        public static void TestLineIntersection()
        {
            var a = new LineSegment2D(new Vector2D(0, 0), new Vector2D(1, 1));
            var b = new LineSegment2D(new Vector2D(0, 1), new Vector2D(1, 0));
            Console.WriteLine(LineSegment2D.Intersection(a, b));

            a = new LineSegment2D(new Vector2D(0, 0), new Vector2D(1, 1));
            b = new LineSegment2D(new Vector2D(0, 0), new Vector2D(1, 1));
            Console.WriteLine(LineSegment2D.Intersection(a, b));

            a = new LineSegment2D(new Vector2D(0, 0), new Vector2D(1, 1));
            b = new LineSegment2D(new Vector2D(0, -1), new Vector2D(1, -1));
            Console.WriteLine(LineSegment2D.Intersection(a, b));
        }

        public static void TestTriTri()
        {
            TriangleTriangleVolumeCalculator calc = new TriangleTriangleVolumeCalculator();

            var a = new Triangle3D(new []
            {
                new Vertex3D(new Vector3D(0, 0, 0), 0),
                new Vertex3D(new Vector3D(1, 0, 0), 1),
                new Vertex3D(new Vector3D(0, 1, 0), 2),
            });

            var b = new Triangle3D(new[]
            {
                new Vertex3D(new Vector3D(-1000, 0.98, 1), 0),
                new Vertex3D(new Vector3D(0.98, -998, 1), 1),
                new Vertex3D(new Vector3D(0.98, 0.98, 1), 2),
            });

            var ret = calc.Compute(a, b);
            calc.Dump();
            Console.WriteLine("+{0:0.000} -{1:0.000} [{2}]", calc.PositiveVolume, calc.NegativeVolume, ret);
        }

        public static void TestTriTriIntersect()
        {
            TriangleTriangleVolumeCalculator calc = new TriangleTriangleVolumeCalculator();

            var a = new Triangle3D(new[]
            {
                new Vertex3D(new Vector3D(0, 0, 0), 0),
                new Vertex3D(new Vector3D(1, 0, 0), 1),
                new Vertex3D(new Vector3D(0, 1, 0), 2),
            });

            var b = new Triangle3D(new[]
            {
                new Vertex3D(new Vector3D(0.001, 0.001, -1), 0),
                new Vertex3D(new Vector3D(0.997, 0.00548, -1), 1),
                new Vertex3D(new Vector3D(0.002578, 1.001254, 1), 2),
            });

            var ret = calc.Compute(a, b);
            calc.Dump();
            Console.WriteLine("+{0:0.000} -{1:0.000} [{2}]", calc.PositiveVolume, calc.NegativeVolume, ret);

            ret = calc.Compute(a, b);
            calc.Dump();
            Console.WriteLine("+{0:0.000} -{1:0.000} [{2}]", calc.PositiveVolume, calc.NegativeVolume, ret);
        }

        public static void TestTriTriDistict()
        {
            TriangleTriangleVolumeCalculator calc = new TriangleTriangleVolumeCalculator();

            var a = new Triangle3D(new[]
            {
                new Vertex3D(new Vector3D(0, 0, 0), 0),
                new Vertex3D(new Vector3D(1, 0, 0), 1),
                new Vertex3D(new Vector3D(0, 1, 0), 2),
            });

            var b = new Triangle3D(new[]
            {
                new Vertex3D(new Vector3D(10, 11, -1), 0),
                new Vertex3D(new Vector3D(10, 55, -1), 1),
                new Vertex3D(new Vector3D(10, 32, 1), 2),
            });

            var ret = calc.Compute(a, b);
            calc.Dump();
            Console.WriteLine("+{0:0.000} -{1:0.000} [{2}]", calc.PositiveVolume, calc.NegativeVolume, ret);

            ret = calc.Compute(a, b);
            calc.Dump();
            Console.WriteLine("+{0:0.000} -{1:0.000} [{2}]", calc.PositiveVolume, calc.NegativeVolume, ret);
        }
    }
}
