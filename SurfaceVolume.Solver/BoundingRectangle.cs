﻿namespace SurfaceVolume2
{
    using SurfaceVolume2.Math;
    using System.Collections.Generic;

    public class BoundingRectangle
    {
        public Vector2D BottomLeft { get; private set; }
        public Vector2D TopRight { get; private set; }

        public LineSegment2D[] Edges { get; private set; }

        void MakeEdges()
        {
            Edges = new[]             
            { 
                new LineSegment2D(BottomLeft, new Vector2D(BottomLeft.X, TopRight.Y)),
                new LineSegment2D(BottomLeft, new Vector2D(TopRight.X, BottomLeft.Y)),
                new LineSegment2D(TopRight, new Vector2D(TopRight.X, BottomLeft.Y)),
                new LineSegment2D(TopRight, new Vector2D(BottomLeft.X, TopRight.Y))
            };
        }

        public BoundingRectangle(Vector2D bottomLeft, Vector2D topRight)
        {
            BottomLeft = bottomLeft;
            TopRight = topRight;
            MakeEdges();
        }

        public BoundingRectangle(IEnumerable<Vector2D> xs)
        {
            double blX = double.MaxValue, blY = double.MaxValue;
            double trX = double.MinValue, trY = double.MinValue;
            foreach (var v in xs)
            {
                blX = System.Math.Min(blX, v.X);
                blY = System.Math.Min(blY, v.Y);
                trX = System.Math.Max(trX, v.X);
                trY = System.Math.Max(trY, v.Y);
            }

            BottomLeft = new Vector2D(blX, blY);
            TopRight = new Vector2D(trX, trY);
            MakeEdges();
        }

        public bool PointInRect(Vector2D point)
        {
            return (point.X >= BottomLeft.X && point.X <= TopRight.X)
                && (point.Y >= BottomLeft.Y && point.Y <= TopRight.Y);
        }

        public bool TriangleIntersects(Triangle3D t)
        {
            var vertices = t.ProjectedVertices;

            // Check if the triangle is outside the boundary.
            for (int i = 0; i < 3; i++)
            {
                var v = vertices[i];
                if (PointInRect(v))
                {
                    return true;
                }
            }

            // Check if the boundary is inside the triangle
            // It is enough to check one point, otherwise they intersect on the edges and the problem is resolved in the next step,
            // which needs to be done anyway.
            if (t.PointInTriangle(BottomLeft)) return true;

            // Check if the boundary and triangle edges intersect.
            for (int i = 0; i < 3; i++)
            {
                var a = t.ProjectedEdges[i];
                for (int j = 0; j < 4; j++)
                {
                    var b = Edges[j];
                    if (LineSegment2D.Intersection(a, b).HasValue) return true;
                }
            }

            return false;
        }

        public static BoundingRectangle Intersect(BoundingRectangle a, BoundingRectangle b)
        {
            if (a.BottomLeft.X > b.TopRight.X
             || b.BottomLeft.X > a.TopRight.X
             || a.BottomLeft.Y > b.TopRight.Y
             || b.BottomLeft.Y > a.TopRight.Y)
            {
                return null;
            }

            return new BoundingRectangle(
                new Vector2D(System.Math.Max(a.BottomLeft.X, b.BottomLeft.X), System.Math.Max(a.BottomLeft.Y, b.BottomLeft.Y)),
                new Vector2D(System.Math.Min(a.TopRight.X, b.TopRight.X), System.Math.Min(a.TopRight.Y, b.TopRight.Y)));
        }
    }
}