﻿
namespace SurfaceVolume2
{
    static class Constants
    {
        /// <summary>
        /// Used to convert double coordinate to long and back;
        /// </summary>
        public const double DoubleToLongFactor = 1000.0;

        /// <summary>
        /// Used in zero-testing.
        /// </summary>
        public const double Epsilon = 0.0000001;

        /// <summary>
        /// Used in line intersections and point in triangle tests.
        /// </summary>
        public const double Tolerance = 0.0000001;

        /// <summary>
        /// Used in Tri-Tri-Volume AddPoint
        /// </summary>
        public const double MinPointSquaredDistance = 0.000005;
    }
}
