﻿namespace SurfaceVolume2
{
    using SurfaceVolume2.Collections;
    using SurfaceVolume2.Math;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    static class PointSimilarityHandler
    {
        public static ClipperLib.IntPoint ToIntPoint(Vector2D point)
        {
            return new ClipperLib.IntPoint((long)(point.X * Constants.DoubleToLongFactor), (long)(point.Y * Constants.DoubleToLongFactor));
        }

        public static Vector2D ToVector2D(ClipperLib.IntPoint point)
        {
            return new Vector2D((double)point.X / Constants.DoubleToLongFactor, (double)point.Y / Constants.DoubleToLongFactor);
        }

        public static Vector3D Snap2D(Vector3D point)
        {
            if ((double)long.MaxValue / System.Math.Abs(point.X) < Constants.DoubleToLongFactor
                || (double)long.MaxValue / System.Math.Abs(point.Y) < Constants.DoubleToLongFactor)
            {
                throw new ArgumentException(string.Format("Vector {0} is too large and cannot be used by the Clipper library.", point));
            }

            long x = (long)(point.X * Constants.DoubleToLongFactor), y = (long)(point.Y * Constants.DoubleToLongFactor);
            return new Vector3D((double)x / Constants.DoubleToLongFactor, (double)y / Constants.DoubleToLongFactor, point.Z);
        }

        public static void AddPoint(Vector2D[] vertices, Vector2D pt, ref int counter)
        {
            // check if a similar point have already been added.
            for (int i = 0; i < counter; i++)
            {
                var v = vertices[i];
                if (pt.DistanceToSquared(v) < Constants.MinPointSquaredDistance)
                {
                    // In case of collision, use the lexicographically smaller vertex.
                    if (pt.X < v.X || (pt.X == v.X && pt.Y < v.Y))
                    {
                        vertices[i] = pt;
                    }
                    return;
                }
            }
            vertices[counter++] = pt;
        }
    }

    class CWComparer : IComparer<Vector2D>
    {
        public Vector2D Center;

        public int Compare(Vector2D x, Vector2D y)
        {
            var v1 = System.Math.Atan2(x.Y - Center.Y, x.X - Center.X);
            //if (v1 < 0) v1 = 2 * System.Math.PI - v1;
            var v2 = System.Math.Atan2(y.Y - Center.Y, y.X - Center.X);
            //if (v2 < 0) v2 = 2 * System.Math.PI - v2;
            return v1.CompareTo(v2);
        }
    }

    class SegmentTree<T>
    {
        public abstract class RangeWrap : IRangeProvider<double>, IEquatable<RangeWrap>
        {
            readonly int id;
            readonly Range<double> range;
            public readonly T Segment;
            
            protected RangeWrap(T segment, Range<double> range, int id)
            {
                this.id = id;
                this.range = range;
                this.Segment = segment;
            }

            public override int GetHashCode()
            {
                return id;
            }

            public override bool Equals(object obj)
            {
                var o = obj as RangeWrap;
                if (o == null) return false;
                return id == o.id;
            }

            public bool Equals(RangeWrap other)
            {
                return id == other.id;
            }

            public Range<double> Range
            {
                get { return range; }
            }
        }

        class XRangeWrap : RangeWrap
        {
            public XRangeWrap(T segment, Func<T, Vector2D> left, Func<T, Vector2D> right, int id)
                : base(segment, new Range<double>(System.Math.Min(left(segment).X, right(segment).X), System.Math.Max(left(segment).X, right(segment).X)), id)
            {
            }
        }

        class YRangeWrap : RangeWrap
        {
            public YRangeWrap(T segment, Func<T, Vector2D> left, Func<T, Vector2D> right, int id)
                : base(segment, new Range<double>(System.Math.Min(left(segment).Y, right(segment).Y), System.Math.Max(left(segment).Y, right(segment).Y)), id)
            {
            }
        }

        class RangeComparer : IComparer<RangeWrap>
        {
            public int Compare(RangeWrap x, RangeWrap y)
            {
                return x.Range.CompareTo(y.Range);
            }
        }

        readonly RangeTree<double, RangeWrap> XRanges, YRanges;

        public HashSet<RangeWrap> GetSegmentsInRange(double minX, double maxX, double minY, double maxY)
        {
            var xSegments = XRanges.Query(new Range<double>(minX - Constants.Tolerance, maxX + Constants.Tolerance));
            var ySegments = YRanges.Query(new Range<double>(minY - Constants.Tolerance, maxY + Constants.Tolerance));
            var segments = new HashSet<RangeWrap>(xSegments);
            segments.IntersectWith(ySegments);
            return segments;
        }

        public HashSet<RangeWrap> GetSegmentsInRange(LineSegment2D segment)
        {
            return GetSegmentsInRange(
                System.Math.Min(segment.A.X, segment.B.X),
                System.Math.Max(segment.A.X, segment.B.X),
                System.Math.Min(segment.A.Y, segment.B.Y),
                System.Math.Max(segment.A.Y, segment.B.Y));
        }

        public SegmentTree(IList<T> segments, Func<T, Vector2D> left, Func<T, Vector2D> right)
        {
            XRanges = new RangeTree<double, RangeWrap>(segments.Select((e, i) => new XRangeWrap(e, left, right, i)), new RangeComparer());
            YRanges = new RangeTree<double, RangeWrap>(segments.Select((e, i) => new YRangeWrap(e, left, right, i)), new RangeComparer());
        }
    }
}
