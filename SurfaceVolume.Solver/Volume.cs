﻿namespace SurfaceVolume2
{
    using SurfaceVolume2.Math;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using SurfaceVolume2.Export;

    /* The steps of the algorithm:
     * 
     * 1) Clip the input meshes using the specified polygon
     * 2) Find two triangles that intersect (along the boundary of both meshes)
     * 3) Compute the volume for each pair of triangles that have some intersection.
     * 
     */

    public class VolumeComputationResult
    {
        public double PositiveVolume { get; set; }
        public double NegativeVolume { get; set; }
    }

    class VolumeCalculator
    {
        public struct IdPair : IEquatable<IdPair>
        {
            readonly int X, Y, Hash;
            
            public IdPair(int x, int y)
            {
                this.X = x;
                this.Y = y;
                // a good hash always helps...
                long i = X, j = Y;
                long key = (i << 32) | j;
                key = (~key) + (key << 18); // key = (key << 18) - key - 1;
                key = key ^ (key >> 31);
                key = key * 21; // key = (key + (key << 2)) + (key << 4);
                key = key ^ (key >> 11);
                key = key + (key << 6);
                key = key ^ (key >> 22);
                this.Hash = (int)key;
            }

            public override int GetHashCode()
            {
                return Hash;
            }

            public override bool Equals(object obj)
            {
                if (obj is IdPair) return Equals((IdPair)obj);
                return false;
            }

            public bool Equals(IdPair other)
            {
                return this.X == other.X && this.Y == other.Y;
            }
        }

        public Mesh3D A { get; private set; }
        public Mesh3D B { get; private set; }
        public Polygon[] Polygons { get; private set; }

        public Exporter Exporter { get; private set; }

        bool createExporter;
        TriangleTriangleVolumeCalculator triCalculator;

        double positiveVolume, negativeVolume;

        HashSet<IdPair> visited = new HashSet<IdPair>();

        bool ShouldVisit(Triangle3D a, Triangle3D b)
        {
            return visited.Add(new IdPair(a.Id, b.Id));
        }

        Tuple<Triangle3D,Triangle3D> FindPivots(ConnectedMesh3D a, ConnectedMesh3D b)
        {
            // Complexity:
            // m = a.ProjectedBoundary.Length
            // n = b.ProjectedBoundary.Length
            // N = min(m, n), M = max(m, n)
            // K = a.Triangle.Count
            // L = b.Triangle.Count
            // 
            // O( M * log M + N * log M + K + L )
            
            // Find the intersection of the boundary.
            BoundingRectangle boundary = BoundingRectangle.Intersect(a.Boundary, b.Boundary);
            if (boundary == null) return null;
                        
            var xs = a.ProjectedBoundary;
            var ys = b.ProjectedBoundary;

            if (xs.Length * ys.Length < 10000)
            {
                // Check the if the boundary intersects...
                for (int i = 0; i < xs.Length; i++)
                {
                    var x = xs[i];

                    //if (!boundary.PointInRect(x.Item1.A) && !boundary.PointInRect(x.Item1.B)) continue;
                    for (int j = 0; j < ys.Length; j++)
                    {
                        var y = ys[j];
                        //if (!boundary.PointInRect(y.Item1.A) && !boundary.PointInRect(y.Item1.B)) continue;
                        if (LineSegment2D.Intersection(x.Item1, y.Item1).HasValue)
                        {
                            return Tuple.Create(x.Item2, y.Item2);
                        }
                    }
                }
            }
            else
            {
                // ...use a more sophisticaled method for large number of edges
                Tuple<LineSegment2D, Triangle3D>[] pivots;
                SegmentTree<Tuple<LineSegment2D, Triangle3D>> segmentLookup;
                bool flipped;

                if (xs.Length <= ys.Length)
                {
                    flipped = false;
                    pivots = xs;
                    segmentLookup = new SegmentTree<Tuple<LineSegment2D, Triangle3D>>(ys, e => e.Item1.A, e => e.Item1.B);
                }
                else
                {
                    flipped = true;
                    pivots = ys;
                    segmentLookup = new SegmentTree<Tuple<LineSegment2D, Triangle3D>>(xs, e => e.Item1.A, e => e.Item1.B);
                }

                foreach (var x in pivots)
                {
                    foreach (var y in segmentLookup.GetSegmentsInRange(x.Item1))
                    {
                        if (LineSegment2D.Intersection(x.Item1, y.Segment.Item1).HasValue)
                        {
                            return flipped
                                ? Tuple.Create(y.Segment.Item2, x.Item2)
                                : Tuple.Create(x.Item2, y.Segment.Item2);
                        }
                    }
                }
            }

            // If the boundary does not intersect, there is still a change one surface is fully within the other 
            // Pick a "random" vertex and try to find a corresponding triangle in the other suface for it.
            var pivot3D = a.Triangles[0].Vertices[0];
            var pivot2D = new Vector2D(pivot3D.Position.X, pivot3D.Position.Y);
            foreach (var t in b.Triangles)
            {
                if (t.PointInTriangle(pivot2D)) return Tuple.Create(a.Triangles[0], t);
            }

            pivot3D = b.Triangles[0].Vertices[0];
            pivot2D = new Vector2D(pivot3D.Position.X, pivot3D.Position.Y);
            foreach (var t in a.Triangles)
            {
                if (t.PointInTriangle(pivot2D)) return Tuple.Create(t, b.Triangles[0]);
            }

            NotFound++;
            return null;
        }

        int VisitedCount = 0, NotFound = 0;

        void Traverse(ConnectedMesh3D a, ConnectedMesh3D b)
        {
            // 1. Find two triangles that intersect, add them to a queue
            // 2. While process queue is not empty:
            //    a) Compute volume between the two triangles
            //    b) If there is some sort of intersection (can still be zero volume as the intersection can be just 1 vertex) 
            //       between currenly processed pair [X,Y], add 6 pairs [X, Y.Adjacency(i)], [X.Adjacency(i), Y] to the process queue

            Queue<Tuple<Triangle3D, Triangle3D>> queue = new Queue<Tuple<Triangle3D, Triangle3D>>();
            
            var pivots = FindPivots(a, b);

            // no intersection => zero volume.
            if (pivots == null) return;

            queue.Enqueue(pivots);
            ShouldVisit(pivots.Item1, pivots.Item2);

            while (queue.Count > 0)
            {
                VisitedCount++;

                var current = queue.Dequeue();
                var x = current.Item1;
                var y = current.Item2;

                if (triCalculator.Compute(x, y))
                {
                    Debug.Assert(triCalculator.PositiveVolume >= 0);
                    Debug.Assert(triCalculator.NegativeVolume >= 0);

                    positiveVolume += triCalculator.PositiveVolume;
                    negativeVolume += triCalculator.NegativeVolume;

                    var adj = y.Adjacency;
                    var mask = triCalculator.AdjacencyMaskB;
                    for (int i = 0; i < 3; i++)
                    {
                        var t = adj[i];
                        if (t != null && (mask & (1 << i)) > 0 && ShouldVisit(x, t)) queue.Enqueue(Tuple.Create(x, t));    
                    }

                    adj = x.Adjacency;
                    mask = triCalculator.AdjacencyMaskA;
                    for (int i = 0; i < 3; i++)
                    {
                        var t = adj[i];
                        if (t != null && (mask & (1 << i)) > 0 && ShouldVisit(t, y)) queue.Enqueue(Tuple.Create(t, y));
                    }
                }
            }
        }

        /// <summary>
        /// Computes the volume between two meshes.
        /// </summary>
        /// <returns></returns>
        public VolumeComputationResult Compute()
        {
            // Complexity:
            //
            // O ( complexity of FindPivots + # of pairs of triangles that intersect )
            // # of pairs of triangles that intersect = O( number of triangles )

            var aClip = Polygon.Intersect(A.Boundary, Polygons);
            var bClip = Polygon.Intersect(B.Boundary, Polygons);
            var clip = Polygon.Intersect(aClip, bClip);
            var clippedA = A.Clip(clip);
            var clippedB = B.Clip(clip); 

            if (createExporter) Exporter = new Exporter(A, B, clippedA, clippedB, Polygons);
            triCalculator = new TriangleTriangleVolumeCalculator(Exporter);
            
            // Compute the volume between all pairs of connected components.
            foreach (var a in clippedA.Components)
            {
                foreach (var b in clippedB.Components)
                {
                    Traverse(a, b);
                }
            }


            if (Exporter != null)
            {
                Exporter.SetVolume(this.positiveVolume, this.negativeVolume);
            }

            return new VolumeComputationResult
            {
                NegativeVolume = this.negativeVolume,
                PositiveVolume = this.positiveVolume
            };
        }

        /// <summary>
        /// Compute volumes between all pairs of triangles.
        /// Only use for testing purposes.
        /// </summary>
        /// <returns></returns>
        public VolumeComputationResult ComputeBruteForce()
        {
            var aClip = Polygon.Intersect(A.Boundary, Polygons);
            var bClip = Polygon.Intersect(B.Boundary, Polygons);
            var clip = Polygon.Intersect(aClip, bClip);
            var clippedA = A.Clip(clip);
            var clippedB = B.Clip(clip);

            if (createExporter) Exporter = new Exporter(A, B, clippedA, clippedB, Polygons);
            triCalculator = new TriangleTriangleVolumeCalculator(Exporter);

            foreach (var a in clippedA.Components)
            {
                foreach (var b in clippedB.Components)
                {
                    foreach (var s in a.Triangles)
                    {
                        foreach (var t in b.Triangles)
                        {
                            if (triCalculator.Compute(s, t))
                            {
                                Debug.Assert(triCalculator.PositiveVolume >= 0);
                                Debug.Assert(triCalculator.NegativeVolume >= 0);

                                positiveVolume += triCalculator.PositiveVolume;
                                negativeVolume += triCalculator.NegativeVolume;
                            }
                        }
                    }
                }
            }

            if (Exporter != null)
            {
                Exporter.SetVolume(this.positiveVolume, this.negativeVolume);
            }

            return new VolumeComputationResult
            {
                NegativeVolume = this.negativeVolume,
                PositiveVolume = this.positiveVolume
            };
        }

        public VolumeCalculator(Mesh3D a, Mesh3D b, IEnumerable<Polygon> polygons, bool createExporter)
        {
            this.createExporter = createExporter;
            A = a;
            B = b;
            Polygons = polygons.ToArray();
        }
    }
}
