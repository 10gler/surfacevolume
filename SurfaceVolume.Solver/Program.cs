﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SurfaceVolume2.Math;
using System.IO;

namespace SurfaceVolume2
{
    class Program
    {
        class ExpectedValue
        {
            public double Plus { get; set; }
            public double Minus { get; set; }
            public double Sum
            {
                get
                {
                    return Plus + Minus;
                }
            }
        }

        static bool ExportVolume;
        static string ExportPath;

        static void DoTest(string name, string exportFilename, Mesh3D meshA, Mesh3D meshB, Polygon polygon, ExpectedValue expectedValue)
        {
            Console.WriteLine("{0}, {1} & {2} triangles", name, meshA.Triangles.Length, meshB.Triangles.Length);
            var sw = Stopwatch.StartNew();
            var calc = new VolumeCalculator(meshA, meshB, new[] { polygon }, ExportVolume);
            var result = calc.Compute();
            sw.Stop();

            Console.WriteLine("Computed: +{0,18:0.000} -{1,18:0.000}", result.PositiveVolume, result.NegativeVolume);
            Console.WriteLine("Expected: +{0,18:0.000} -{1,18:0.000}", expectedValue.Plus, expectedValue.Minus);
            Console.WriteLine("Ratio:    +{0,18:0.000} -{1,18:0.000}", result.PositiveVolume / expectedValue.Plus, result.NegativeVolume / expectedValue.Minus);
            Console.WriteLine("Diff:     +{0,18:0.000} -{1,18:0.000}", System.Math.Abs(result.PositiveVolume - expectedValue.Plus), System.Math.Abs(result.NegativeVolume - expectedValue.Minus));
            Console.WriteLine("Time: {0:0.00}s", sw.Elapsed.TotalSeconds);
            if (ExportVolume)
            {
                Console.WriteLine("Exporting...");
                calc.Exporter.SetExpectedVolume(expectedValue.Plus, expectedValue.Minus);
                var fullExportPath = Path.Combine(ExportPath, exportFilename);
                calc.Exporter.WriteJson(fullExportPath);
            }
            Console.WriteLine("----------------");

        }

        static void DoTesAccuracyAndTimeVsTriangles(Func<double, double, double> f1, BoundingRectangle rect1,
                                                    Func<double, double, double> f2, BoundingRectangle rect2,
            Polygon polygon, ExpectedValue expectedValue, int nxyStart = 10, int nxyEnd = 100, int nxyDelta = 10)
        {
            for (int i = nxyStart; i <= nxyEnd; i += nxyDelta)
            {
                var meshA = GenerateMesh(f1,
                    rect1,
                    i, i);

                var meshB = GenerateMesh(f2,
                    rect1,
                    i, i);

                var sw = Stopwatch.StartNew();
                var calc = new VolumeCalculator(meshA, meshB, new[] { polygon }, ExportVolume);
                var result = calc.Compute();
                sw.Stop();

                Console.Write("Time: {0:0.000}s ", sw.Elapsed.TotalSeconds);
                Console.Write("Triangles: {0} & {1}\t", meshA.Triangles.Length, meshB.Triangles.Length);
                Console.Write("Computed: +{0:0.000} -{1:0.000}\t", result.PositiveVolume, result.NegativeVolume);
                Console.Write("Expected: +{0:0.000} -{1:0.000}\t", expectedValue.Plus, expectedValue.Minus);
                Console.Write("Error: +{0:F4}% -{1:F4}%\t", System.Math.Abs((expectedValue.Plus - result.PositiveVolume)) / expectedValue.Plus * 100, System.Math.Abs((System.Math.Abs(expectedValue.Minus) - System.Math.Abs(result.NegativeVolume))) / System.Math.Abs(expectedValue.Minus) * 100);
                Console.Write("Ratio: +{0:0.000} -{1:0.000}\t", result.PositiveVolume / expectedValue.Plus, result.NegativeVolume / expectedValue.Minus);
                Console.WriteLine("Diff: +{0:000} -{1:000}\t", System.Math.Abs(result.PositiveVolume - expectedValue.Plus), System.Math.Abs(result.NegativeVolume - expectedValue.Minus));
            }
        }

        static Tuple<string, Mesh3D> GenerateMesh(string xmlFileName)
        {
            Dictionary<Vector3D, Vertex3D> vertices = new Dictionary<Vector3D, Vertex3D>();
            XElement surfaceRoot = XElement.Load(xmlFileName);
            int counter = 0;


            foreach (var v in surfaceRoot.Elements("t").Elements("v"))
            {
                var p = new Vector3D(double.Parse(v.Attribute("x").Value, CultureInfo.InvariantCulture), double.Parse(v.Attribute("y").Value, CultureInfo.InvariantCulture),
                            double.Parse(v.Attribute("z").Value, CultureInfo.InvariantCulture));

                if (!vertices.ContainsKey(p))
                {
                    vertices.Add(p, new Vertex3D(p, counter++));
                }
            }

            var tris = surfaceRoot.Elements("t")
                .Select((t, i) => new Triangle3D(t.Elements("v")
                    .Select(v =>
                    {
                        var p = new Vector3D(double.Parse(v.Attribute("x").Value, CultureInfo.InvariantCulture), double.Parse(v.Attribute("y").Value, CultureInfo.InvariantCulture),
                        double.Parse(v.Attribute("z").Value, CultureInfo.InvariantCulture));
                        return vertices[p];
                    })))
                .ToArray();

            var sw = Stopwatch.StartNew();
            var mesh = Mesh3D.Create(tris);
            sw.Stop();
            Console.WriteLine("Mesh '{0}' generated in {1:0.00}s.", new FileInfo(xmlFileName).Name, sw.Elapsed.TotalSeconds);

            return Tuple.Create(new FileInfo(xmlFileName).Name, mesh);
        }

        static void SurfacesFromXmlTest(Tuple<string, Mesh3D> meshA, Tuple<string, Mesh3D> meshB, Polygon polygon, ExpectedValue expected, string exportFilename)
        {
            DoTest(string.Format("{0} / {1}, {2} clip vertices", meshA.Item1, meshB.Item1, polygon.Vertices.Length), exportFilename, meshA.Item2, meshB.Item2, polygon, expected);
        }

        static Polygon GetPolygonFromXmlFile(string fileName)
        {
            XElement poly = XElement.Load(fileName);
            var q = poly.Elements("v").Select(n =>
                new Vector2D(double.Parse(n.Attribute("x").Value, CultureInfo.InvariantCulture), double.Parse(n.Attribute("y").Value, CultureInfo.InvariantCulture)));
            return new Polygon(q);
        }

        static Mesh3D GenerateMesh(Func<double, double, double> f, BoundingRectangle rect, int nX, int nY)
        {
            var dX = (rect.TopRight.X - rect.BottomLeft.X) / (nX - 1);
            var dY = (rect.TopRight.Y - rect.BottomLeft.Y) / (nY - 1);

            int count = 0;
            var xs = new Vertex3D[nX][];

            for (int i = 0; i < nX; i++)
            {
                xs[i] = new Vertex3D[nY];
                var x = rect.BottomLeft.X + i * dX;
                for (int j = 0; j < nY; j++)
                {
                    var y = rect.BottomLeft.Y + j * dY;
                    xs[i][j] = new Vertex3D(new Vector3D(x, y, f(x, y)), count++);
                }
            }

            var tris = new List<Triangle3D>();

            count = 0;
            for (int i = 0; i < nX - 1; i++)
            {
                for (int j = 0; j < nY - 1; j++)
                {
                    tris.Add(new Triangle3D(new Vertex3D[] { xs[i][j], xs[i + 1][j], xs[i + 1][j + 1] }));
                    tris.Add(new Triangle3D(new Vertex3D[] { xs[i][j], xs[i][j + 1], xs[i + 1][j + 1] }));
                }
            }

            return Mesh3D.Create(tris);
        }

        static void Main(string[] args)
        {
            ExportVolume = false;
            //ExportPath = @"I:\Projects\Misc\surfacevolume\tests";  
            ExportPath = "@./../../../../SurfaceVolume.Visualizator/ExportResults/";

            var inputPath = @"./../../XmlData/";

            ////Tests.TestTriTriIntersect();
            ////////Tests.TestTriTri();
            ////////Tests.TestTriTriDistict();
            ////return;

            //var meshA = GenerateMesh(inputPath + "surfaceA.xml");
            //var meshB = GenerateMesh(inputPath + "surfaceB.xml");
            //var meshC = GenerateMesh(inputPath + "surfaceC.xml");
            //var meshD = GenerateMesh(inputPath + "surfaceD.xml");

            //Console.WriteLine("------------------");

            //Polygon poly;
            //ExpectedValue expected;

            //poly = Polygon.Infinite();
            //expected = new ExpectedValue { Minus = 9435, Plus = 3846778928 };
            //SurfacesFromXmlTest(meshB, meshA, poly, expected, "b_a.json");

            //expected = new ExpectedValue { Plus = 9435, Minus = 3846778928 };
            //SurfacesFromXmlTest(meshA, meshB, poly, expected, "a_b.json");

            //expected = new ExpectedValue { Minus = 4706932.0311, Plus = 87041412.7612 };
            //SurfacesFromXmlTest(meshD, meshC, poly, expected, "d_c.json");

            //expected = new ExpectedValue { Plus = 4706932.0311, Minus = 87041412.7612 };
            //SurfacesFromXmlTest(meshC, meshD, poly, expected, "c_d.json");

            ////return;

            //poly = GetPolygonFromXmlFile(inputPath + "polygon.xml");

            //expected = new ExpectedValue { Plus = 0, Minus = 453414405.3989 };
            //SurfacesFromXmlTest(meshA, meshB, poly, expected, "a_b_clip.json");
            ////VolumeDebug.WriteMathematicaTriangles();

            ////return;

            //expected = new ExpectedValue { Minus = 0, Plus = 453414405.3989 };
            //SurfacesFromXmlTest(meshB, meshA, poly, expected, "b_a_clip.json");

            //expected = new ExpectedValue { Minus = 140333.8045, Plus = 3918723.1774 };
            //SurfacesFromXmlTest(meshD, meshC, poly, expected, "d_c_clip.json");

            //expected = new ExpectedValue { Plus = 140333.8045, Minus = 3918723.1774 };
            //SurfacesFromXmlTest(meshC, meshD, poly, expected, "c_d_clip.json");
            ////VolumeDebug.WriteMathematicaTriangles();

            ////end real examples            

            #region Testing surfaecs

            ////Volume between z = sinx * siny and z = 0
            //var poly = Polygon.Infinite();

            //var mesh = GenerateMesh((x, y) => System.Math.Sin(x) * System.Math.Sin(y),
            //    new BoundingRectangle(new Vector2D(0.0, 0.0), new Vector2D(2 * System.Math.PI, 2 * System.Math.PI)),
            //    100, 100);

            //var planeMesh = GenerateMesh((x, y) => 0,
            //    new BoundingRectangle(new Vector2D(-16, -16), new Vector2D(16, 16)),
            //    2, 2);
            //var expected = new ExpectedValue { Minus = 16 / 2, Plus = 16 / 2 };
            //DoTest("sin mesh", "sin-plane.json", mesh, planeMesh, poly, expected);

            ////Volume between z = sinx * siny, z = 0 and limited polygon
            //const double pi = System.Math.PI;
            //poly = new Polygon(new[] { new Vector2D(pi / 2.0, pi / 2.0), new Vector2D(pi, pi / 2.0), new Vector2D(pi, pi), new Vector2D(pi / 2.0, pi) });
            //mesh = GenerateMesh((x, y) => System.Math.Sin(x) * System.Math.Sin(y),
            //    new BoundingRectangle(new Vector2D(0.0, 0.0), new Vector2D(2 * System.Math.PI, 2 * System.Math.PI)),
            //    100, 100);

            //planeMesh = GenerateMesh((x, y) => 0,
            //    new BoundingRectangle(new Vector2D(-16, -16), new Vector2D(16, 16)),
            //    2, 2);
            //expected = new ExpectedValue { Minus = 0, Plus = 1 };
            //DoTest("sin mesh and polygon", "sin-plane-polygon.json", mesh, planeMesh, poly, expected);

            //Volume inside z = x^2 + y^2 and z = 16
            var poly = Polygon.Infinite();
            var mesh = GenerateMesh((x, y) => System.Math.Pow(x, 2.0) + System.Math.Pow(y, 2.0),
                new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
                100, 100);

            var planeMesh = GenerateMesh((x, y) => 16,
                new BoundingRectangle(new Vector2D(-16, -16), new Vector2D(16, 16)),
                50, 50);
            var expected = new ExpectedValue { Minus = 0, Plus = 402.12 };
            DoTest("cup", "cup.json", planeMesh, mesh, poly, expected);

            //Volume limited by z = x^2 + y^2, z = 16, and polygon 
            //var poly = new Polygon(new[] { new Vector2D(-1, -1), new Vector2D(1, -1), new Vector2D(1, 1), new Vector2D(-1, 1) });
            //var mesh = GenerateMesh((x, y) => System.Math.Pow(x, 2.0) + System.Math.Pow(y, 2.0),
            //    new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //    100, 100);

            //var planeMesh = GenerateMesh((x, y) => 16,
            //    new BoundingRectangle(new Vector2D(-16, -16), new Vector2D(16, 16)),
            //    2, 2);
            //var expected = new ExpectedValue { Minus = 0, Plus = 64 - 8.0 / 3.0 };
            //DoTest("cup-polygon", "cup-polygon.json", planeMesh, mesh, poly, expected);

            //Volume limited by z = xy + 40, z = y^2 - x^2 + 10, and polygon 
            //var noTri = 50;
            //var poly = new Polygon(new[] { new Vector2D(-3, -3), new Vector2D(3, -3), new Vector2D(-3, 3) });
            //var mesh1 = GenerateMesh((x, y) => System.Math.Pow(y, 2.0) - System.Math.Pow(x, 2.0) + 10,
            //    new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //    noTri, noTri);

            //var mesh2 = GenerateMesh((x, y) => x * y + 40,
            //    new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //    noTri, noTri);
            //var expected = new ExpectedValue { Minus = 0, Plus = 720 - 180 };
            //DoTest("2-surface", "2-surfaces.json", mesh2, mesh1, poly, expected);

            ////Volume limited by z = xy + 40, z = y^2 - x^2 + 10, and polygon 2
            //noTri = 100;
            //var poly2 = ArticleExaplesHelper.GetPolyDoubleQuadraticFunction(100);
            //poly2.SavePolygonToXml(inputPath + "poly2.xml");
            //mesh1 = GenerateMesh((x, y) => System.Math.Pow(y, 2.0) - System.Math.Pow(x, 2.0) + 10,
            //    new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //    noTri, noTri);

            //mesh2 = GenerateMesh((x, y) => x * y + 40,
            //    new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //    noTri, noTri);
            //expected = new ExpectedValue { Minus = 0, Plus = 713.8286 };
            //DoTest("2-surface, poly2", "2-surfaces-poly2.json", mesh2, mesh1, poly2, expected);


            //Volume limited by z = xy + 40, z = y^2 - x^2 + 10, and hexagon
            //int noTri = 100;
            //var poly2 = ArticleExaplesHelper.GetHexagon();
            //poly2.SavePolygonToXml(inputPath + "hexagon.xml");
            //var mesh1 = GenerateMesh((x, y) => System.Math.Pow(y, 2.0) - System.Math.Pow(x, 2.0) + 10,
            //    new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //    noTri, noTri);

            //var mesh2 = GenerateMesh((x, y) => x * y + 40,
            //    new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //    noTri, noTri);
            //var expected = new ExpectedValue { Minus = 0, Plus = 356.6667 };
            //DoTest("2-surface, hexagon", "2-surfaces-hexagon.json", mesh2, mesh1, poly2, expected);

            #endregion

            //Volume limited by z = xy + 40, z = y^2 - x^2 + 10, and hexagon

            //DoTesAccuracyAndTimeVsTriangles((x, y) => x * y + 40,
            //     new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //     (x, y) => System.Math.Pow(y, 2.0) - System.Math.Pow(x, 2.0) + 10,
            //     new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //     ArticleExaplesHelper.GetHexagon(),
            //     new ExpectedValue { Minus = 0, Plus = 356.6667 }, 10, 500, 10);

            //var poly = new Polygon(new[] { new Vector2D(-3, -3), new Vector2D(3, -3), new Vector2D(-3, 3) });
            //DoTesAccuracyAndTimeVsTriangles((x, y) => x * y + 40,
            //        new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //        (x, y) => System.Math.Pow(y, 2.0) - System.Math.Pow(x, 2.0) + 10,
            //        new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //        poly,
            //        new ExpectedValue { Minus = 0, Plus = 720 - 180 }, 100, 100, 10);

            //var poly = new Polygon(new[] { new Vector2D(-3, -3), new Vector2D(3, -3), new Vector2D(-3, 3) });
            //DoTesAccuracyAndTimeVsTriangles((x, y) => x * y + 40,
            //        new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //        (x, y) => System.Math.Pow(y, 2.0) - System.Math.Pow(x, 2.0) + 10,
            //        new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //        poly,
            //        new ExpectedValue { Minus = 0, Plus = 720 - 180}, 10, 36, 1);

            //Volume inside z = x^2 + y^2 and z = y + 16 (plane 45 degrees to labe xy and goes tru point (0, 0, 16))
            //mesh = GenerateMesh((x, y) => System.Math.Pow(x, 2.0) + System.Math.Pow(y, 2.0),
            //    new BoundingRectangle(new Vector2D(-4, -4), new Vector2D(4, 4)),
            //    100, 100);

            //planeMesh = GenerateMesh((x, y) => y + 16,
            //    new BoundingRectangle(new Vector2D(-16, -16), new Vector2D(16, 16)),
            //    2, 2);
            //expected = new ExpectedValue { Minus = 0 / 2, Plus = 402.12 };
            //DoTest("xy mesh", "xy.json", planeMesh, mesh, poly, expected);

            ////var sinMesh = GenerateMesh((x, y) => Math.Sin(x) * Math.Sin(y),
            ////    new BoundingRectangle { BottomLeft = new Vector2D(0.0, 0.0), TopRight = new Vector2D(32 * Math.PI, 32 * Math.PI) },
            ////    350, 250);
            ////var planeMesh = GenerateMesh((x, y) => 0,
            ////    new BoundingRectangle { BottomLeft = new Vector2D(-2000, -1235), TopRight = new Vector2D(2000, 1258) },
            ////    200, 200);
            ////var expected = new ExpectedValue { Minus = 256 * 8, Plus = 256 * 8 };
            ////DoTest("sin mesh", sinMesh, planeMesh, expected);
        }
    }
}
