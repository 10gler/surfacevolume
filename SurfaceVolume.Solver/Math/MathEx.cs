﻿namespace SurfaceVolume2.Math
{
    public static partial class MathEx
    {        
        public static double DistanceTo(this Vector3D point, Vector3D other)
        {
            double x = point.X - other.X;
            double y = point.Y - other.Y;
            double z = point.Z - other.Z;

            return System.Math.Sqrt(x * x + y * y + z * z);
        }

        public static double DistanceToSquared(this Vector3D point, Vector3D other)
        {
            double x = point.X - other.X;
            double y = point.Y - other.Y;
            double z = point.Z - other.Z;

            return x * x + y * y + z * z;
        }

        public static double DistanceToSquared(this Vector2D point, Vector2D other)
        {
            double x = point.X - other.X;
            double y = point.Y - other.Y;

            return x * x + y * y ;
        }
    }
}