﻿namespace SurfaceVolume2.Math
{
    public struct Vector2D
    {
        public readonly double X;
        public readonly double Y;

        public Vector2D(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        #region Properties

        public double Length
        {
            get
            {
                return System.Math.Sqrt(this.X * this.X + this.Y * this.Y);
            }
        }

        public double LengthSquared
        {
            get
            {
                return this.X * this.X + this.Y * this.Y;
            }
        }

        #endregion

        #region Methods

        public override int GetHashCode()
        {
            int hash = 31;
            hash = unchecked(23 * hash + X.GetHashCode());
            hash = unchecked(23 * hash + Y.GetHashCode());
            return hash;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Vector2D))
            {
                return false;
            }

            return Equals(this, (Vector2D)obj);
        }

        public bool Equals(Vector2D v)
        {
            return Equals(this, v);
        }

        public static bool Equals(Vector2D v1, Vector2D v2)
        {
            //TODO: Use this?
            /*
            return MathHelper.AreEqual(v1.x, v2.x) &&
                   MathHelper.AreEqual(v1.y, v2.y)*/

            return v1.X.Equals(v2.X) &&
                   v1.Y.Equals(v2.Y);
        }

        public override string ToString()
        {
            return X + ", " + Y;
        }

        public static double DotProduct(Vector2D v1, Vector2D v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y;
        }
                
        public static Vector2D Add(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.X + v2.X, v1.Y + v2.Y);
        }
        
        public static Vector2D operator +(Vector2D v1, Vector2D v2)
        {
            return Add(v1, v2);
        }
        
        public static Vector2D Subtract(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.X - v2.X, v1.Y - v2.Y);
        }
        
        public static Vector2D operator -(Vector2D v1, Vector2D v2)
        {
            return Subtract(v1, v2);
        }

        public static Vector2D Multiply(double scalar, Vector2D v)
        {
            return new Vector2D(v.X * scalar, v.Y * scalar);
        }

        public static Vector2D Multiply(Vector2D v, double scalar)
        {
            return new Vector2D(v.X * scalar, v.Y * scalar);
        }

        public static Vector2D operator *(double scalar, Vector2D v)
        {
            return Multiply(scalar, v);
        }

        public static Vector2D operator *(Vector2D v, double scalar)
        {
            return Multiply(v, scalar);
        }

        public static Vector2D Divide(Vector2D v, double scalar)
        {
            double div = 1.0 / scalar;
            return new Vector2D(v.X * div, v.Y * div);
        }

        public static Vector2D operator /(Vector2D v, double scalar)
        {
            return Divide(v, scalar);
        }

        public static bool operator ==(Vector2D v1, Vector2D v2)
        {
            return Equals(v1, v2);
        }

        public static bool operator !=(Vector2D v1, Vector2D v2)
        {
            return !Equals(v1, v2);
        }

        public static Vector2D operator -(Vector2D v)
        {
            return new Vector2D(-v.X, -v.Y);
        }

        #endregion
    }
}