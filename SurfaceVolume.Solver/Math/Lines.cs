﻿namespace SurfaceVolume2.Math
{
    /// <summary>
    /// Can represent line or line segment (A = Offset, B = Offset + Direction)
    /// </summary>
    public struct Line2D
    {
        public readonly Vector2D Normal;
        public readonly double D;

        public readonly Vector2D A, B;

        public Line2D(Vector2D a, Vector2D b)
        {
            this.A = a;
            this.B = b;

            var n = b - a;
            this.Normal = new Vector2D(n.Y, -n.X);
            this.D = -Vector2D.DotProduct(a, this.Normal);
        }
        
        public Line2D From3DPoints(Vector3D a, Vector3D b)
        {
            return new Line2D(new Vector2D(a.X, a.Y), new Vector2D(b.X, b.Y));
        }

        public bool IsLeft(Vector2D point)
        {
            return point.X * Normal.X + point.Y * Normal.Y + D < 0.0;
        }
    }

    /// <summary>
    /// Can represent line or line segment (A = Offset, B = Offset + Direction)
    /// </summary>
    public struct LineSegment2D
    {
        public readonly Vector2D A, B;

        public LineSegment2D(Vector2D a, Vector2D b)
        {
            this.A = a;
            this.B = b;
        }

        public static LineSegment2D FromEdge3D(Edge3D edge)
        {
            return new LineSegment2D(new Vector2D(edge.A.Position.X, edge.A.Position.Y), new Vector2D(edge.B.Position.X, edge.B.Position.Y));
        }

        public static Vector2D? Intersection(LineSegment2D x, LineSegment2D y)
        {
            Vector2D a = x.A, b = x.B, c = y.A, d = y.B;
            double bx = b.X - a.X;
            double by = b.Y - a.Y;
            double dx = d.X - c.X;
            double dy = d.Y - c.Y;
            double b_dot_d_perp = bx * dy - by * dx;
            if (System.Math.Abs(b_dot_d_perp) <= Constants.Epsilon)
            {
                return null;
            }
            double cx = c.X - a.X;
            double cy = c.Y - a.Y;
            double t = (cx * dy - cy * dx) / b_dot_d_perp;
            //if (t < 0 || t > 1)
            if (t < -Constants.Tolerance || t > 1 + Constants.Tolerance)
            {
                return null;
            }
            double u = (cx * by - cy * bx) / b_dot_d_perp;
            //if (u < 0 || u > 1)
            if (u < -Constants.Tolerance || u > 1 + Constants.Tolerance)
            {
                return null;
            }
            return new Vector2D(a.X + t * bx, a.Y + t * by);
        }

        public static Vector2D? Intersection(LineSegment2D segment, Line2D line)
        {
            Vector2D a = segment.A, b = segment.B, c = line.A, d = line.B;
            double bx = b.X - a.X;
            double by = b.Y - a.Y;
            double dx = d.X - c.X;
            double dy = d.Y - c.Y;
            double b_dot_d_perp = bx * dy - by * dx;
            //if (b_dot_d_perp == 0)
            if (System.Math.Abs(b_dot_d_perp) <= Constants.Epsilon)
            {
                return null;
            }
            double cx = c.X - a.X;
            double cy = c.Y - a.Y;
            double t = (cx * dy - cy * dx) / b_dot_d_perp;
            //if (t < 0 || t > 1)
            if (t < -Constants.Tolerance || t > 1 + Constants.Tolerance)
            {
                return null;
            }
            ////double u = (cx * by - cy * bx) / b_dot_d_perp;
            ////if (u < 0 || u > 1)
            ////{
            ////    return null;
            ////}
            return new Vector2D(a.X + t * bx, a.Y + t * by);
        }
    }    
}
