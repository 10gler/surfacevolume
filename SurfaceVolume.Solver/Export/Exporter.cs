﻿using Newtonsoft.Json;
using SurfaceVolume2.Math;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurfaceVolume2.Export
{
    public class Exporter
    {
        List<VolumeTriangle> volumes = new List<VolumeTriangle>();
        Mesh3D meshA, meshB;
        ClippedMesh3D clippedA, clippedB;
        Polygon[] clipPolygons;
        double postiveVolume, negativeVolume;
        double expPostiveVolume, expNegativeVolume;

        ExportableSurface ToSurface(IEnumerable<Triangle3D> triangles)
        {
            var xs = triangles.ToList();
            var vs = triangles.SelectMany(t => t.Vertices).Distinct().ToList();
            var vsMap = vs.Select((v, i) => new { V = v, I = i }).ToDictionary(v => v.V, v => v.I);

            return new ExportableSurface
            {
                Vertices = vs.Select(v => new[] { v.Position.X, v.Position.Y, v.Position.Z }).ToArray(),
                Triangles = xs.Select(t => new[] { vsMap[t.Vertices[0]], vsMap[t.Vertices[1]], vsMap[t.Vertices[2]] }).ToArray()
            };        
        }

        ExportablePolygon ToPolygon(IEnumerable<Vector2D> xs)
        {
            return new ExportablePolygon 
            {
                Vertices = xs.Select(v => new [] { v.X, v.Y }).ToArray()
            };
        }

        public void AddVolume(Vector2D[] vertices, double[] zA, double[] zB, double volume)
        {
            volumes.Add(new VolumeTriangle
            {
                Vertices = vertices.Select(v => new [] { v.X, v.Y }).ToArray(),
                ZValuesA = zA.ToArray(),
                ZValuesB = zB.ToArray(),
                Volume = volume
            });
        }

        public void SetVolume(double positiveVolume, double negativeVolume)
        {
            this.postiveVolume = positiveVolume;
            this.negativeVolume = negativeVolume;
        }

        public void SetExpectedVolume(double positiveVolume, double negativeVolume)
        {
            this.expPostiveVolume = positiveVolume;
            this.expNegativeVolume = negativeVolume;
        }

        public void WriteJson(string filename)
        {
            var ret = new ExportableResult
            {
                SurfaceA = ToSurface(meshA.Triangles),
                SurfaceB = ToSurface(meshB.Triangles),
                ClippedSurfaceA = ToSurface(clippedA.Components.SelectMany(c => c.Triangles)),
                ClippedSurfaceB = ToSurface(clippedB.Components.SelectMany(c => c.Triangles)),
                ClipPolygons = clipPolygons.Select(p => ToPolygon(p.Vertices)).ToArray(),
                NegativeVolume = negativeVolume,
                PositiveVolume = postiveVolume,
                ExpectedNegativeVolume = expNegativeVolume,
                ExpectedPositiveVolume = expPostiveVolume,
                Volumes = volumes.ToArray()
            };

            File.WriteAllText(filename, JsonConvert.SerializeObject(ret));
        }

        public Exporter(Mesh3D meshA, Mesh3D meshB, ClippedMesh3D clippedA, ClippedMesh3D clippedB, Polygon[] clipPolygons)
        {
            this.meshA = meshA;
            this.meshB = meshB;
            this.clippedA = clippedA;
            this.clippedB = clippedB;
            this.clipPolygons = clipPolygons.Where(p => !p.IsInfinite()).ToArray();
        }
    }
}
