﻿namespace SurfaceVolume2
{
    using SurfaceVolume2.Collections;
    using SurfaceVolume2.Math;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Stores a position and a unique identifier of a vertex.
    /// </summary>
    public class Vertex3D
    {
        public Vector3D Position { get; private set; }
        public int Id { get; private set; }

        /// <summary>
        /// Is must be unique.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="id"></param>
        public Vertex3D(Vector3D position, int id)
        {
            this.Position = PointSimilarityHandler.Snap2D(position);
            this.Id = id;
        }

        /// <summary>
        /// Hash Code = id.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Id;
        }

        /// <summary>
        /// Id based equality.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var o = obj as Vertex3D;
            if (o == null) return false;
            return o.Id == this.Id;
        }
    }

    /// <summary>
    /// Helper class to construct triangle adjacency.
    /// </summary>
    public class Edge3D
    {
        /// <summary>
        /// Vertex with the lower id.
        /// </summary>
        public Vertex3D A { get; private set; }

        /// <summary>
        /// Vertex with the higher id.
        /// </summary>
        public Vertex3D B { get; private set; }

        int hash;

        public Edge3D(Vertex3D a, Vertex3D b)
        {
            if (a.Id < b.Id)
            {
                this.A = a;
                this.B = b;
            }
            else
            {
                this.B = a;
                this.A = b;
            }

            // a good hash always helps...
            long i = A.Id, j = B.Id;
            long key = (i << 32) | j;
            key = (~key) + (key << 18); // key = (key << 18) - key - 1;
            key = key ^ (key >> 31);
            key = key * 21; // key = (key + (key << 2)) + (key << 4);
            key = key ^ (key >> 11);
            key = key + (key << 6);
            key = key ^ (key >> 22);
            this.hash = (int)key;
        }

        public override int GetHashCode()
        {
            return hash;
        }

        public override bool Equals(object obj)
        {
            var o = obj as Edge3D;
            if (o == null) return false;
            return o.A.Id == A.Id && o.B.Id == B.Id;
        }
    }
        
    public class Triangle3D : IEquatable<Triangle3D>
    {
        /// <summary>
        /// This is set in Polygon.Clip
        /// </summary>
        internal int Id { get; set; }

        public Vertex3D[] Vertices { get; private set; }
        
        /// <summary>
        /// Adjaceny[i] = triangle that does not contain Vertices[i], null if boundary
        /// </summary>
        public List<Triangle3D> Adjacency { get; private set; }

        public Vector2D[] ProjectedVertices { get; private set; }
        public LineSegment2D[] ProjectedEdges { get; private set; }

        bool isGegenerate;
        double degenerateHeight;

        /// <summary>
        /// helper values for baricentric intepolation.
        /// </summary>
        double invDet;
        double dot00, dot01, dot11;
        Vector2D a;
        Vector2D v0, v1;

        /// <summary>
        /// Helper for value interpolation. 
        /// Might be an overkill (coz of the barycentric stuff) but 
        /// I was too laze to think.
        /// </summary>
        Vector3D normal;
        double d;

        public Triangle3D(IEnumerable<Vertex3D> vs)
        {
            this.Vertices = vs.ToArray();
            this.Adjacency = new List<Triangle3D>(3);

            double cX = 0, cY = 0;
            for (int i = 0; i < 3; i++)
            {
                cX += Vertices[i].Position.X;
                cY += Vertices[i].Position.Y;
            }

            a = new Vector2D(Vertices[0].Position.X, Vertices[0].Position.Y);
            v0 = new Vector2D(Vertices[2].Position.X - Vertices[0].Position.X, Vertices[2].Position.Y - Vertices[0].Position.Y);
            v1 = new Vector2D(Vertices[1].Position.X - Vertices[0].Position.X, Vertices[1].Position.Y - Vertices[0].Position.Y);
            dot00 = Vector2D.DotProduct(v0, v0);
            dot01 = Vector2D.DotProduct(v0, v1);
            dot11 = Vector2D.DotProduct(v1, v1);

            invDet = 1 / (dot00 * dot11 - dot01 * dot01);

            normal = Vector3D.CrossProduct(Vertices[2].Position - Vertices[0].Position, Vertices[1].Position - Vertices[0].Position).Normalize();
            d = -Vector3D.DotProduct(normal, Vertices[0].Position);

            isGegenerate = System.Math.Abs(normal.Z) < Constants.Epsilon;
            degenerateHeight = (Vertices[0].Position.Z + Vertices[1].Position.Z + Vertices[2].Position.Z) / 3.0;

            ProjectedVertices = new Vector2D[]
            {
                new Vector2D(Vertices[0].Position.X, Vertices[0].Position.Y),
                new Vector2D(Vertices[1].Position.X, Vertices[1].Position.Y),
                new Vector2D(Vertices[2].Position.X, Vertices[2].Position.Y)
            };

            ProjectedEdges = new LineSegment2D[]
            {
                new LineSegment2D(ProjectedVertices[1], ProjectedVertices[2]),
                new LineSegment2D(ProjectedVertices[0], ProjectedVertices[2]),
                new LineSegment2D(ProjectedVertices[0], ProjectedVertices[1]),
            };
        }

        /// <summary>
        /// Call this only at the end of mesh initialization.
        /// </summary>
        public void NormalizeAdjacency()
        {
            var normalized = new List<Triangle3D>(3) { null, null, null };
            for (int i = 0; i < 3; i++)
            {
                var a = Vertices[i == 0 ? 1 : 0];
                var b = Vertices[i <= 1 ? 2 : 1];

                for (int j = 0; j < Adjacency.Count; j++)
                {
                    var n = Adjacency[j];
                    if (Array.IndexOf(n.Vertices, a) >= 0 && Array.IndexOf(n.Vertices, b) >= 0)
                    {
                        normalized[i] = n;
                        break;
                    }
                }
            }
            Adjacency = normalized;
        }

        /// <summary>
        /// only needed temporary.
        /// </summary>
        /// <returns></returns>
        public Edge3D[] GetEdges()
        {
            return new Edge3D[] 
            {
                new Edge3D(Vertices[1], Vertices[2]),
                new Edge3D(Vertices[0], Vertices[2]),
                new Edge3D(Vertices[0], Vertices[1]),
            };
        }
        
        public double InterpolateZ(Vector2D point)
        {
            if (isGegenerate) return degenerateHeight;
            return -(point.X * normal.X + point.Y * normal.Y + d) / normal.Z;
        }

        public bool PointInTriangle(Vector2D point)
        {
            var v2 = point - a;
            var dot02 = Vector2D.DotProduct(v0, v2);
            var dot12 = Vector2D.DotProduct(v1, v2);

            var u = (dot11 * dot02 - dot01 * dot12) * invDet;
            var v = (dot00 * dot12 - dot01 * dot02) * invDet;

            return (u >= -Constants.Tolerance) && (v >= -Constants.Tolerance) && (u + v <= 1 + Constants.Tolerance);
        }

        public static Line2D ProjectedPlaneIntersection(Triangle3D a, Triangle3D b)
        {
            var na = a.normal;
            var nb = b.normal;

            var dot = Vector3D.DotProduct(na, nb);
            // The planes are almost parallel, so return "infinite plane"
            if (System.Math.Abs(dot) >= 1 - Constants.Epsilon)
            {
                return new Line2D(new Vector2D(double.PositiveInfinity, double.NegativeInfinity), new Vector2D(double.PositiveInfinity, double.PositiveInfinity));
            }

            // Find a single point and the direction.
            var dir = Vector3D.CrossProduct(na, nb);
            var denom = 1 / (1 - dot * dot);
            double c1 = (a.d - b.d * dot) * denom, c2 = (b.d - a.d * dot) * denom;
            var pt = -(c1 * na + c2 * nb);
            return new Line2D(new Vector2D(pt.X, pt.Y), new Vector2D(pt.X + dir.X, pt.Y + dir.Y));
        }

        public bool Equals(Triangle3D other)
        {
            return Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public override bool Equals(object obj)
        {
            var o = obj as Triangle3D;
            if (o == null) return false;
            return o.Id == Id;
        }
    }

    /// <summary>
    /// A mesh representation.
    /// </summary>
    public class ConnectedMesh3D
    {
        public Triangle3D[] Triangles { get; private set; }
        public Tuple<LineSegment2D, Triangle3D>[] ProjectedBoundary { get; private set; }
        public BoundingRectangle Boundary { get; private set; }
        
        /// <summary>
        /// Creating the mesh fills in the triangle adjacency.
        /// </summary>
        /// <param name="triangles"></param>
        /// <returns></returns>
        public static ConnectedMesh3D Create(IEnumerable<Triangle3D> triangles, ILookup<Triangle3D, Edge3D> boundary)
        {
            var tris = triangles.ToArray();            
            
            return new ConnectedMesh3D
            {
                Triangles = tris,
                ProjectedBoundary = tris
                    .Where(t => t.Adjacency.Count != 3)
                    .SelectMany(t => boundary[t]
                        .Select(e =>
                            Tuple.Create(
                                new LineSegment2D(new Vector2D(e.A.Position.X, e.A.Position.Y), new Vector2D(e.B.Position.X, e.B.Position.Y)), 
                                t)))
                    .ToArray(),
                Boundary = new BoundingRectangle(tris.SelectMany(t => t.Vertices).Select(v => new Vector2D(v.Position.X, v.Position.Y)))
            };
        }

        private ConnectedMesh3D()
        {

        }
    }

    /// <summary>
    /// A mesh representation.
    /// </summary>
    public class ClippedMesh3D
    {
        public ConnectedMesh3D[] Components { get; private set; }
        public int TriangleCount { get; private set; }
                
        public ClippedMesh3D(IEnumerable<ConnectedMesh3D> components)
        {
            this.Components = components.ToArray();
            this.TriangleCount = components.Sum(c => c.Triangles.Length);
        }
    }

    /// <summary>
    /// A mesh representation.
    /// </summary>
    public class Mesh3D
    {
        public Triangle3D[] Triangles { get; private set; }
        public Polygon[] Boundary { get; private set; }
                
        static List<Triangle3D> FindComponent(Triangle3D pivot, HashSet<Triangle3D> visited)
        {
            // Breath-first search for the connected component

            var component = new List<Triangle3D>();
            var visitQueue = new Queue<Triangle3D>();

            visitQueue.Enqueue(pivot);
            visited.Add(pivot);

            while (visitQueue.Count > 0)
            {
                var t = visitQueue.Dequeue();
                component.Add(t);

                foreach (var n in t.Adjacency)
                {
                    if (!visited.Contains(n))
                    {
                        visitQueue.Enqueue(n);
                        visited.Add(n);
                    }
                }
            }

            return component;
        }

        static List<Edge3D> GetConnectedEdgeComponent(Edge3D pivot, HashSet<Edge3D> visited, Dictionary<Vertex3D, Edge3D[]> vertexEdges)
        {
            var component = new List<Edge3D>();
            var visitQueue = new Queue<Edge3D>();

            visitQueue.Enqueue(pivot);
            visited.Add(pivot);
            while (visitQueue.Count > 0)
            {
                var e = visitQueue.Dequeue();
                component.Add(e);
                foreach (var n in vertexEdges[e.A])
                {
                    if (visited.Add(n)) visitQueue.Enqueue(n);
                }
                foreach (var n in vertexEdges[e.B])
                {
                    if (visited.Add(n)) visitQueue.Enqueue(n);
                }
            }

            return component;
        }

        static bool IsClosed(List<Edge3D> edges, Dictionary<Vertex3D, Edge3D[]> vertexEdges)
        {
            return edges.All(e => vertexEdges[e.A].Length == 2) && edges.All(e => vertexEdges[e.B].Length == 2);
        }

        static Polygon GetBoundary(List<Triangle3D> tris, ILookup<Triangle3D, Edge3D> edgeLookup)
        {
            var edges = tris
                    .Where(t => t.Adjacency.Count != 3)
                    .SelectMany(t => edgeLookup[t])
                    .ToArray();
            
            var vertexEdges = edges
                .ToLookup(e => e.A)
                .ToDictionary(g => g.Key, g => g.ToArray());

            foreach (var p in edges.ToLookup(e => e.B))
            {
                if (!vertexEdges.ContainsKey(p.Key)) vertexEdges.Add(p.Key, p.ToArray());
                else
                {
                    vertexEdges[p.Key] = p.Union(vertexEdges[p.Key]).ToArray();
                }
            }

            var components = new List<List<Edge3D>>();
            var visited = new HashSet<Edge3D>();
            while (true)
            {
                var pivot = edges.FirstOrDefault(e => !visited.Contains(e));
                if (pivot == null) break;
                components.Add(GetConnectedEdgeComponent(pivot, visited, vertexEdges));
            }

            var closed = components.Where(c => IsClosed(c, vertexEdges)).ToList();
            if (closed.Count != 1)
            {
                throw new InvalidOperationException("The mesh is not in a valid format.");
            }

            var singles = vertexEdges.Where(e => e.Value.Length == 1).ToArray();
            foreach (var s in singles)
            {
                Console.WriteLine("{0} - {1}", s.Value[0].A.Id, s.Value[0].B.Id);
            }
                        
            
            List<Vertex3D> orderedPoints = new List<Vertex3D>();

            var component = components[0];
            var currentEdge = edges[0];
            var currentPoint = currentEdge.A;
            while (orderedPoints.Count != component.Count)
            {
                orderedPoints.Add(currentPoint);
                currentPoint = currentEdge.A == currentPoint ? currentEdge.B : currentEdge.A;
                var edge = vertexEdges[currentPoint];
                currentEdge = object.ReferenceEquals(edge[0], currentEdge) ? edge[1] : edge[0];
            }

            return new Polygon(orderedPoints.Select(p => new Vector2D(p.Position.X, p.Position.Y)));
        }

        /// <summary>
        /// Returns a clipped mesh.
        /// Assumes the polygons do no intersect!!!
        /// </summary>
        /// <param name="polygons"></param>
        /// <returns></returns>
        public ClippedMesh3D Clip(Polygon[] clip)
        {
            var maxVertexId = Triangles.Max(t => System.Math.Max(t.Vertices[0].Id, System.Math.Max(t.Vertices[1].Id, t.Vertices[2].Id))) + 1;
            var tris = new List<Triangle3D>();
            Dictionary<Vector2D, Vertex3D> addedVertices = new Dictionary<Vector2D, Vertex3D>();

            foreach (var polygon in clip)
            {
                foreach (var tri in Triangles)
                {
                    polygon.Clip(tri, tris, addedVertices, ref maxVertexId);
                }
            }
            
            foreach (var tri in tris) tri.Adjacency.Clear();

            Dictionary<Edge3D, Triangle3D> adj = new Dictionary<Edge3D, Triangle3D>(3 * tris.Count);
            foreach (var t in tris)
            {
                var es = t.GetEdges();
                for (int i = 0; i < 3; i++)
                {
                    var e = es[i];
                    Triangle3D at;
                    if (adj.TryGetValue(e, out at))
                    {
                        if (at == null) continue;
                        at.Adjacency.Add(t);
                        t.Adjacency.Add(at);
                        adj[e] = null;
                    }
                    else
                    {
                        adj.Add(e, t);
                    }
                }
            }

            // Helper for ConnectedMesh.Create
            var boundaryLookup = adj.Where(e => e.Value != null).ToLookup(e => e.Value, e => e.Key);

            var components = new List<List<Triangle3D>>();
            var visited = new HashSet<Triangle3D>();
            while (true)
            {
                Triangle3D pivot = tris.FirstOrDefault(t => !visited.Contains(t));
                if (pivot == null) break;
                components.Add(FindComponent(pivot, visited));
            }

            var ret = new ClippedMesh3D(components.Select(c => ConnectedMesh3D.Create(c, boundaryLookup)));

            // must be called AFTER the connected components were created.
            foreach (var t in tris)
            {
                t.NormalizeAdjacency();
            }

            return ret;
        }

        /// <summary>
        /// Creating the mesh fills in the triangle adjacency.
        /// </summary>
        /// <param name="triangles"></param>
        /// <returns></returns>
        public static Mesh3D Create(IEnumerable<Triangle3D> triangles)
        {
            var tris = triangles.ToArray();
            for (int i = 0; i < tris.Length; i++)
            {
                tris[i].Id = i;
            }
            
            // Compute triangle adjacency
            Dictionary<Edge3D, Triangle3D> adj = new Dictionary<Edge3D, Triangle3D>(3 * tris.Length);
            foreach (var t in tris)
            {
                var es = t.GetEdges();
                for (int i = 0; i < 3; i++)
                {
                    var e = es[i];
                    Triangle3D at;
                    if (adj.TryGetValue(e, out at))
                    {
                        if (at == null) continue;
                        at.Adjacency.Add(t);
                        t.Adjacency.Add(at);
                        adj[e] = null;
                    }
                    else
                    {
                        adj.Add(e, t);
                    }
                }
            }
            
            // Helper for ConnectedMesh.Create
            var boundaryLookup = adj.Where(e => e.Value != null).ToLookup(e => e.Value, e => e.Key);

            List<Polygon> boundary = new List<Polygon>();
            var visited = new HashSet<Triangle3D>();
            while (true)
            {
                Triangle3D pivot = tris.FirstOrDefault(t => !visited.Contains(t));
                if (pivot == null) break;
                boundary.Add(GetBoundary(FindComponent(pivot, visited), boundaryLookup));
            }

            var ret = new Mesh3D
            {
                Triangles = tris,
                Boundary = boundary.ToArray()
            };

            return ret;
        }

        private Mesh3D()
        {

        }
    }
}
