﻿namespace SurfaceVolume2.Collections
{
    using SurfaceVolume2.Math;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents a priority/value pair.
    /// </summary>
    class Vector2DValuePair<TValue>
    {
        /// <summary>
        /// Position.
        /// </summary>
        public readonly Vector2D Position;

        /// <summary>
        /// Value.
        /// </summary>
        public readonly TValue Value;

        /// <summary>
        /// Creates the pair.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="value"></param>
        public Vector2DValuePair(Vector2D position, TValue value)
        {
            this.Position = position;
            this.Value = value;
        }
    }

    /// <summary>
    /// Compares the given coordinate...
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    class CoordinateComparer2D<TValue> : IComparer<Vector2DValuePair<TValue>>
    {
        Func<Vector2DValuePair<TValue>, double> selector;

        public int Compare(Vector2DValuePair<TValue> a, Vector2DValuePair<TValue> b)
        {
            return selector(a).CompareTo(selector(b));
        }

        public CoordinateComparer2D(Func<Vector2DValuePair<TValue>, double> selector)
        {
            this.selector = selector;
        }
    }

}
