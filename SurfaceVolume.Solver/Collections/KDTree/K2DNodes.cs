﻿namespace SurfaceVolume2.Collections
{
    using SurfaceVolume2.Math;

    abstract class K2DNode<TValue>
    {
        public abstract void Nearest(Vector2D pivot, double maxDistance, double maxDistanceSquared, IPriorityCollection<double, TValue> output);
        //public abstract void Nearest(Vector3D pivot, K3DRect rect, double maxDistanceSquared, IPriorityCollection<double, TValue> output);
        public abstract void Nearest(Vector2D pivot, K2DRect rect, int n, IPriorityArray<double, TValue> output);
    }

    abstract class K2DSplitNode<TValue> : K2DNode<TValue>
    {
        public K2DNode<TValue> LeftChild { get; internal set; }
        public K2DNode<TValue> RightChild { get; internal set; }
                
        readonly double SplitValue;

        protected abstract double GetCoordinate(Vector2D vector);
        protected abstract void SetRectBounds(K2DRect rect, double min, double max);
        protected abstract void SetRectBoundsNext(K2DRect rect, double min, double max);
        protected abstract double GetRectMin(K2DRect rect);
        protected abstract double GetRectMax(K2DRect rect);
        protected abstract double GetRectMinNext(K2DRect rect);
        protected abstract double GetRectMaxNext(K2DRect rect);

        bool Intersects(double v, double radius)
        {
            if (v <= SplitValue)
            {
                if (v + radius > SplitValue) return true;
                return false;
            }
            else
            {
                if (v - radius < SplitValue) return true;
                return false;
            }
        }

        public override void Nearest(Vector2D pivot, double maxDistance, double maxDistanceSquared, IPriorityCollection<double, TValue> output)
        {
            var coord = GetCoordinate(pivot);

            if (Intersects(coord, maxDistance))
            {
                LeftChild.Nearest(pivot, maxDistance, maxDistanceSquared, output);
                RightChild.Nearest(pivot, maxDistance, maxDistanceSquared, output);
            }
            else
            {
                if (coord <= SplitValue) LeftChild.Nearest(pivot, maxDistance, maxDistanceSquared, output);
                else RightChild.Nearest(pivot, maxDistance, maxDistanceSquared, output);
            }
        }

        //public override void Nearest(Vector3D pivot, K3DRect rect, double maxDistanceSquared, IPriorityCollection<double, TValue> output)
        //{
        //    bool onLeft = GetCoordinate(pivot) <= SplitValue;

        //    // store the next level bounds
        //    double min = GetRectMinNext(rect), max = GetRectMaxNext(rect);

        //    if (onLeft)
        //    {
        //        var old = GetRectMax(rect);
        //        SetRectBounds(rect, GetRectMin(rect), SplitValue);
        //        LeftChild.Nearest(pivot, rect, maxDistanceSquared, output);
        //        SetRectBounds(rect, SplitValue, old);
        //    }
        //    else
        //    {
        //        var old = GetRectMin(rect);
        //        SetRectBounds(rect, SplitValue, GetRectMax(rect));
        //        RightChild.Nearest(pivot, rect, maxDistanceSquared, output);
        //        SetRectBounds(rect, old, SplitValue);
        //    }

        //    // restore the next level bounds
        //    SetRectBoundsNext(rect, min, max);

        //    var furthest = output.GetLargestPriority(maxDistanceSquared);
        //    var closest = rect.DistanceSquaredToClosestEdge(pivot);
            
        //    if (closest <= furthest)
        //    {
        //        // store the next level bounds
        //        min = GetRectMinNext(rect);
        //        max = GetRectMaxNext(rect);

        //        if (onLeft) RightChild.Nearest(pivot, rect, furthest, output);
        //        else LeftChild.Nearest(pivot, rect, furthest, output);

        //        // restore the next level bounds
        //        SetRectBoundsNext(rect, min, max);
        //    }
        //}

        public override void Nearest(Vector2D pivot, K2DRect rect, int n, IPriorityArray<double, TValue> output)
        {
            bool onLeft = GetCoordinate(pivot) <= SplitValue;

            // store the next level bounds
            double min = GetRectMinNext(rect), max = GetRectMaxNext(rect);

            if (onLeft)
            {
                var old = GetRectMax(rect);
                SetRectBounds(rect, GetRectMin(rect), SplitValue);
                LeftChild.Nearest(pivot, rect, n, output);
                SetRectBounds(rect, SplitValue, old);
            }
            else
            {
                var old = GetRectMin(rect);
                SetRectBounds(rect, SplitValue, GetRectMax(rect));
                RightChild.Nearest(pivot, rect, n, output);
                SetRectBounds(rect, old, SplitValue);
            }

            // restore the next level bounds
            SetRectBoundsNext(rect, min, max);

            var furthest = n == output.Count 
                ? output.GetLargestPriority(double.MaxValue) 
                : double.MaxValue;
            var closest = rect.DistanceSquaredToClosestEdge(pivot);

            if (closest <= furthest)
            {
                // store the next level bounds
                min = GetRectMinNext(rect);
                max = GetRectMaxNext(rect);

                if (onLeft) RightChild.Nearest(pivot, rect, n, output);
                else LeftChild.Nearest(pivot, rect, n, output);

                // restore the next level bounds
                SetRectBoundsNext(rect, min, max);
            }
        }

        protected K2DSplitNode(double splitValue)
        {
            this.SplitValue = splitValue;
        }
    }

    class XSplitNode<TValue> : K2DSplitNode<TValue>
    {
        protected override double GetCoordinate(Vector2D vector) { return vector.X; }
        protected override void SetRectBounds(K2DRect rect, double min, double max) { rect.MinX = min; rect.MaxX = max; }
        protected override void SetRectBoundsNext(K2DRect rect, double min, double max) { rect.MinY = min; rect.MaxY = max; }
        protected override double GetRectMin(K2DRect rect) { return rect.MinX; }
        protected override double GetRectMax(K2DRect rect) { return rect.MaxX; }
        protected override double GetRectMinNext(K2DRect rect) { return rect.MinY; }
        protected override double GetRectMaxNext(K2DRect rect) { return rect.MaxY; }

        public XSplitNode(double splitValue)
            : base(splitValue)
        {

        }
    }

    class YSplitNode<TValue> : K2DSplitNode<TValue>
    {
        protected override double GetCoordinate(Vector2D vector) { return vector.Y; }
        protected override void SetRectBounds(K2DRect rect, double min, double max) { rect.MinY = min; rect.MaxY = max; }
        protected override void SetRectBoundsNext(K2DRect rect, double min, double max) { rect.MinX = min; rect.MaxX = max; }
        protected override double GetRectMin(K2DRect rect) { return rect.MinY; }
        protected override double GetRectMax(K2DRect rect) { return rect.MaxY; }
        protected override double GetRectMinNext(K2DRect rect) { return rect.MinX; }
        protected override double GetRectMaxNext(K2DRect rect) { return rect.MaxX; }

        public YSplitNode(double splitValue)
            : base(splitValue)
        {

        }
    }
    
    class K2DLeafNode<TValue> : K2DNode<TValue>
    {
        readonly Vector2DValuePair<TValue>[] values;

        public K2DLeafNode(Vector2DValuePair<TValue>[] values)
        {
            this.values = values;
        }

        public K2DLeafNode(Vector2DValuePair<TValue>[] values, int startIndex, int count)
        {
            this.values = new Vector2DValuePair<TValue>[count];
            for (int i = 0; i < count; i++)
            {
                this.values[i] = values[startIndex + i];
            }
        }

        public override void Nearest(Vector2D pivot, double maxDistance, double maxDistanceSquared, IPriorityCollection<double, TValue> output)
        {
            var vals = values;
            for (int i = 0; i < vals.Length; i++)
            {
                var value = vals[i];
                var dist = value.Position.DistanceToSquared(pivot);
                if (dist <= maxDistanceSquared) output.Add(dist, value.Value);
            }
        }

        //public override void Nearest(Vector3D pivot, K3DRect rect, double maxDistanceSquared, IPriorityCollection<double, TValue> output)
        //{
        //    var vals = values;
        //    for (int i = 0; i < vals.Length; i++)
        //    {
        //        var value = vals[i];
        //        var dist = value.Position.DistanceToSquared(pivot);
        //        if (dist <= maxDistanceSquared) output.Add(dist, value.Value);
        //    }
        //}

        public override void Nearest(Vector2D pivot, K2DRect rect, int n, IPriorityArray<double, TValue> output)
        {
            var vals = values;
            var p = output.GetLargestPriority(double.MaxValue);
            for (int i = 0; i < vals.Length; i++)
            {
                var value = vals[i];

                var dist = value.Position.DistanceToSquared(pivot);
                if (output.Count < n || dist < p) output.Add(dist, value.Value);
            }
        }
    }
}
