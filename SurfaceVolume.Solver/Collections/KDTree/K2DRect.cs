﻿
namespace SurfaceVolume2.Collections
{
    using SurfaceVolume2.Math;

    class K2DRect
    {
        public double MinX = double.MinValue, MinY = double.MinValue;
        public double MaxX = double.MaxValue, MaxY = double.MaxValue;

        public double DistanceSquaredToClosestEdge(Vector2D v)
        {
            double x = 0, y = 0;

            if (v.X <= MinX) x = v.X - MinX;
            else if (v.X >= MaxX) x = v.X - MaxX;

            if (v.Y <= MinY) y = v.Y - MinY;
            else if (v.Y >= MaxY) y = v.Y - MaxY;

            return x * x + y * y;
        }
    }
}
