﻿using SurfaceVolume2.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SurfaceVolume2
{
    static public class ArticleExaplesHelper
    {
        public static Polygon GetPolyDoubleQuadraticFunction(int noDivision = 10)
        {
            double dx = 6.0 / (noDivision / 2.0);
            List<Vector2D> res = new List<Vector2D>();
            //above OX
            for (double i = -3; i <= 3; i += dx)
                res.Add(new Vector2D(i, -1.0 / 3 * i * i + 3));

            //bellow OX
            for (double i = 3 - dx; i >= -3; i -= dx)
                res.Add(new Vector2D(i, 1.0 / 3 * i * i - 3));
            return new Polygon(res);
        }

        public static Polygon GetHexagon()
        {
            var res = new List<Vector2D>
            {
                new Vector2D(-2, 0),
                new Vector2D(-1, 2),
                new Vector2D(1, 2),
                new Vector2D(2, 0),
                new Vector2D(1, -2),
                new Vector2D(-1, -2),
                new Vector2D(-2, 0)
            };
            return new Polygon(res);
        }

        public static void SavePolygonToXml(this Polygon poly, string fileName)
        {
            XElement root = new XElement("polygon",
                poly.Vertices.Select(n => new XElement("v", new XAttribute("x", n.X), new XAttribute("y", n.Y))));

            root.Save(fileName);
        }
    }
}
