﻿namespace SurfaceVolume2
{
    using SurfaceVolume2.Export;
    using SurfaceVolume2.Math;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    class TriangleTriangleVolumeCalculator
    {
        Triangle3D A, B;
        double positiveVolume, negativeVolume;

        bool exportTriangles;
        Exporter exporter;

        CWComparer Comparer;

        int IntersectionCount;
        Vector2D[] Intersections;

        int LeftCount, RightCount;
        Vector2D[] LeftVertices, RightVertices;
        double[] ZProjectionsA, ZProjectionsB;

        // The adjacency mask uses 3 bits to determine which neighbor triangles should be visited.
        int adjacencyMaskA, adjacencyMaskB;

        public double PositiveVolume { get { return positiveVolume; } }
        public double NegativeVolume { get { return negativeVolume; } }

        public int AdjacencyMaskA { get { return adjacencyMaskA; } }
        public int AdjacencyMaskB { get { return adjacencyMaskB; } }



        public void Dump()
        {
            // just debugging stuff
            Console.WriteLine("IC: {0}, L: {1}, R: {2}", IntersectionCount, LeftCount, RightCount);
            foreach (var v in Intersections.Take(IntersectionCount)) Console.WriteLine(v);
        }
        
        void Sort(Vector2D[] vertices, int count)
        {
            // sort the vertices in clockwise order (or anticlockwise, but does not really matter)
            double cX = 0, cY = 0;
            for (int i = 0; i < count; i++)
            {
                var v = vertices[i];
                cX += v.X;
                cY += v.Y;
            }
            Comparer.Center = new Vector2D(cX / count, cY / count);
            Array.Sort<Vector2D>(vertices, 0, count, Comparer);
        }

        double TriangleVolume(Vector2D[] vertices, int i, int j, int k)
        {
            // Volume = "area of base triangle" * ("average height of A" - "average height of B")

            Vector2D a = vertices[i], b = vertices[j], c = vertices[k];
            var xy = System.Math.Abs(a.X * (b.Y - c.Y) + b.X * (c.Y - a.Y) + c.X * (a.Y - b.Y));
            var hA = ZProjectionsA[i] + ZProjectionsA[j] + ZProjectionsA[k];
            var hB = ZProjectionsB[i] + ZProjectionsB[j] + ZProjectionsB[k];
            var ret = xy * (hA - hB) / 6.0;
            //VolumeDebug.AddTriangle(ret, vertices[i], vertices[j], vertices[k]);

            if (exportTriangles)
            {
                exporter.AddVolume(
                    new[] { a, b, c },
                    new[] { ZProjectionsA[i], ZProjectionsA[j], ZProjectionsA[k] },
                    new[] { ZProjectionsB[i], ZProjectionsB[j], ZProjectionsB[k] },
                    ret);
            }

            return ret;
        }

        double GetVolume(Vector2D[] vertices, int count)
        {
            // need to sort the vertices if there is more than 3 of them
            if (count > 3)
            {
                Sort(vertices, count);
            }
                        
            // cache the z-projections.
            for (int i = 0; i < count; i++)
            {
                ZProjectionsA[i] = A.InterpolateZ(vertices[i]);
                ZProjectionsB[i] = B.InterpolateZ(vertices[i]);
            }

            double volume = 0;
            for (int i = 1; i < count - 1; i++)
            {
                volume += TriangleVolume(vertices, 0, i, i + 1);
            }

            return volume;
        }
        
        void FindIntersection()
        {
            // Add common vertices from the 1st triangle
            var vert = A.ProjectedVertices;
            for (int i = 0; i < 3; i++)
            {
                var x = vert[i];
                if (B.PointInTriangle(x))
                {
                    PointSimilarityHandler.AddPoint(Intersections, x, ref IntersectionCount);
                    // Add edges adjacent to vertex i. 7 = 111, ^ (xor) removes the i-th bit
                    adjacencyMaskA |= 7 ^ (1 << i);
                }
            }

            // Is the first triangle full in the 2nd?
            if (IntersectionCount == 3) return;
            bool wasZero = IntersectionCount == 0;

            // the second triange
            vert = B.ProjectedVertices;
            for (int i = 0; i < 3; i++)
            {
                var x = vert[i];
                if (A.PointInTriangle(x))
                {
                    PointSimilarityHandler.AddPoint(Intersections, x, ref IntersectionCount);
                    // Add edges adjacent to vertex i. 7 = 111, ^ (xor) removes the i-th bit
                    adjacencyMaskB |= 7 ^ (1 << i);
                }
            }

            // Is the 2nd triangle fully in the first?
            if (wasZero && IntersectionCount == 3) return;

                        
            // Compute intersections of all pairs of edges.
            var eA = A.ProjectedEdges;
            var eB = B.ProjectedEdges;

            for (int i = 0; i < 3; i++)
            {
                var x = eA[i];
                for (int j = 0; j < 3; j++)
                {
                    var y = eB[j];
                    var cross = LineSegment2D.Intersection(x, y);
                    if (cross.HasValue)
                    {
                        PointSimilarityHandler.AddPoint(Intersections, cross.Value, ref IntersectionCount);
                        // Add i/j-th edge.
                        adjacencyMaskA |= 1 << i;
                        adjacencyMaskB |= 1 << j;
                    }
                }
            }
        }

        void Split()
        {
            // Find the plane intersection.
            var plane = Triangle3D.ProjectedPlaneIntersection(A, B);

            // Split the vertices into left and right parts.
            for (int i = 0; i < IntersectionCount; i++)
            {
                var t = Intersections[i];
                if (plane.IsLeft(t)) LeftVertices[LeftCount++] = t;
                else RightVertices[RightCount++] = t;
            }
            
            if (LeftCount > 0 && RightCount > 0)
            {
                Sort(Intersections, IntersectionCount);

                // find intersections of the original edges with the tri-tri intersection
                for (int i = 0; i < IntersectionCount; i++)
                {
                    var segment = new LineSegment2D(Intersections[i], Intersections[(i + 1) % IntersectionCount]);
                    var x = LineSegment2D.Intersection(segment, plane);
                    if (x.HasValue)
                    {
                        PointSimilarityHandler.AddPoint(LeftVertices, x.Value, ref LeftCount);
                        PointSimilarityHandler.AddPoint(RightVertices, x.Value, ref RightCount);
                    }
                }
            }
        }

        void UpdateVolume()
        {
            // Compute volumes of the right and left part.
            if (LeftCount > 2)
            {
                var volume = GetVolume(LeftVertices, LeftCount);
                if (volume > 0) positiveVolume += volume;
                else negativeVolume -= volume;
            }
            if (RightCount > 2)
            {
                var volume = GetVolume(RightVertices, RightCount);
                if (volume > 0) positiveVolume += volume;
                else negativeVolume -= volume;
            }
        }

        void Reset()
        {
            IntersectionCount = LeftCount = RightCount = 0;
            positiveVolume = negativeVolume = 0.0;
            adjacencyMaskA = adjacencyMaskB = 0;
        }
                
        /// <summary>
        /// Both volumes are returned with positive sign.
        /// Returns true if there is any kind of intersection.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="positiveVolume"></param>
        /// <param name="negativeVolume"></param>
        /// <returns></returns>
        public bool Compute(Triangle3D a, Triangle3D b)
        {
            // 1. Find all intersection points between triangles A and B. 
            //    - Only points that are at least Constants.MinPointSquaredDistance from each other are added.
            //      To maintain symmetry (Compute(A, B) = Compute(B, A)), the lexicographically smaller point is used if the collision occurs.
            //    - The result of this step is a 2D polygon that bounds the area where the triangle intersect.
            // 2. Find the intersection X of planes given by triangle A and B, and project it to the XY plane
            //    - Use the projection to split intersected vertices to left and right sets.
            //    - If both left and right sets contains at least 1 vertex, find the two intersections U,V of X and the polygon from step 1. 
            //      Add U and V to both left and right sets.
            // 3. Triangulate left and right sets (if they contain at least 3 vertices) and compute the volume.
            //
            // Complexity O(1) (tho the constant is fairly large)

            // Intersection of A and B details:
            //  - Check if vertices of A are in B
            //  - Check if vertices of B in A
            //  - Computes pairwise edge intersection
            //  - Finds intersection of planes given by A and B and splits the intersections into 2 sets (L, R).
            //  - Both L and R are convex, because intersection of two convex objects is also convex. Sorts the intersection 
            //    points clockwise, triangulate it (creates a triangle fan), and computes the volume.

            A = a;
            B = b;
            
            // Reset the state (counts and volumes to zero)
            Reset();

            FindIntersection();

            // Shared vertex/edge case.
            if (IntersectionCount < 3)
            {
                return IntersectionCount > 0;
            }

            // Check for triangle-trianle intersection.
            Split();

            // Compute the volume
            UpdateVolume();

            return true;
        }

        public TriangleTriangleVolumeCalculator(Exporter exporter = null)
        {
            Comparer = new CWComparer();
            this.exportTriangles = exporter != null;
            this.exporter = exporter;
            int size = 16;
            Intersections = new Vector2D[size];
            LeftVertices = new Vector2D[size];
            RightVertices = new Vector2D[size];
            ZProjectionsA = new double[size];
            ZProjectionsB = new double[size];
        }
    }
}
