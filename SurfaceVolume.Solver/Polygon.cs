﻿namespace SurfaceVolume2
{
    using MIConvexHull;
    using SurfaceVolume2.Collections;
    using SurfaceVolume2.Math;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Polygon
    {
        class TriangulationVertex : IVertex
        {
            public readonly Vector2D Vertex;
            double[] position;
            public TriangulationVertex(Vector2D v)
            {
                Vertex = v;
                position = new[] { v.X, v.Y };
            }

            public double[] Position
            {
                get
                {
                    return position;
                }
                set
                {
                    position = value;
                }
            }
        }

        /// <summary>
        /// Vertices "in order".
        /// </summary>
        public Vector2D[] Vertices { get; private set; }

        readonly BoundingRectangle Boundary;
        readonly SegmentTree<LineSegment2D> SegmentsLookup;
        readonly List<LineSegment2D> Edges;
        readonly List<ClipperLib.IntPoint> ClipperPolygon;

        public static Polygon Infinite()
        {
            return new Polygon(new[] 
            { 
                new Vector2D(double.NegativeInfinity, double.NegativeInfinity),
                new Vector2D(double.PositiveInfinity, double.NegativeInfinity),  
                new Vector2D(double.PositiveInfinity, double.PositiveInfinity),  
                new Vector2D(double.NegativeInfinity, double.PositiveInfinity) 
            });
        }

        /// <summary>
        /// Vertices must be "in order".
        /// </summary>
        /// <param name="pts"></param>
        public Polygon(IEnumerable<Vector2D> vs)
        {
            this.Vertices = vs.ToArray();

            var edges = new List<LineSegment2D>();
            var len = Vertices.Length;
            for (int i = 0; i < Vertices.Length; i++)
            {
                edges.Add(new LineSegment2D(Vertices[i], Vertices[(i + 1) % len]));
            }
            Edges = edges;

            Boundary = new BoundingRectangle(Vertices);
            SegmentsLookup = new SegmentTree<LineSegment2D>(edges, e => e.A, e => e.B);
            ClipperPolygon = ToClipperPolygon(Vertices);
        }
        
        public static Polygon[] Intersect(IEnumerable<Polygon> subjects, IEnumerable<Polygon> clip)
        {
            var sbj = subjects.ToArray();
            var clp = clip.ToArray();

            if (sbj.Any(s => s.IsInfinite())) return clp;
            if (clp.Any(s => s.IsInfinite())) return sbj;
                        
            ClipperLib.Clipper cp = new ClipperLib.Clipper();            
            foreach (var p in sbj) cp.AddPolygon(p.ClipperPolygon, ClipperLib.PolyType.ptSubject);
            foreach (var p in clp) cp.AddPolygon(p.ClipperPolygon, ClipperLib.PolyType.ptClip);

            var solution = new List<List<ClipperLib.IntPoint>>();

            if (cp.Execute(ClipperLib.ClipType.ctIntersection, solution))
            {
                return solution.Select(p => FromClipperPolygon(p)).ToArray();
            }

            return new Polygon[0];
        }

        List<Vector2D[]> Intersect(Vector2D[] points)
        {
            ClipperLib.Clipper cp = new ClipperLib.Clipper();
            cp.AddPolygon(ClipperPolygon, ClipperLib.PolyType.ptSubject);
            cp.AddPolygon(ToClipperPolygon(points), ClipperLib.PolyType.ptClip);

            var solution = new List<List<ClipperLib.IntPoint>>();

            if (cp.Execute(ClipperLib.ClipType.ctIntersection, solution))
            {
                return solution.Select(p => FromClipperPolygonPoints(p)).ToList();
            }

            return new List<Vector2D[]>(0);
        }

        public bool IsInfinite()
        {
            return Vertices.All(v => double.IsInfinity(v.X)) && Vertices.All(v => double.IsInfinity(v.Y));
        }

        static List<ClipperLib.IntPoint> ToClipperPolygon(Vector2D[] vertices)
        {
            return vertices.Select(v => PointSimilarityHandler.ToIntPoint(v)).ToList();
        }

        static Polygon FromClipperPolygon(List<ClipperLib.IntPoint> points)
        {
            return new Polygon(points.Select(p => PointSimilarityHandler.ToVector2D(p)));
        }

        static Vector2D[] FromClipperPolygonPoints(List<ClipperLib.IntPoint> points)
        {
            var ret = new Vector2D[points.Count];
            for (int i = 0; i < points.Count; i++)
            {
                ret[i] = PointSimilarityHandler.ToVector2D(points[i]); 
            }
            return ret;
        }

        bool IsPointInside(Vector2D p)
        {
            Vector2D p1, p2;

            bool inside = false;

            var oldPoint = Vertices[Vertices.Length - 1];

            for (int i = 0; i < Vertices.Length; i++)
            {
                var newPoint = Vertices[i];

                if (newPoint.X > oldPoint.X)
                {
                    p1 = oldPoint;
                    p2 = newPoint;
                }
                else
                {
                    p1 = newPoint;
                    p2 = oldPoint;
                }

                if ((newPoint.X < p.X) == (p.X <= oldPoint.X)
                    && (p.Y - p1.Y) * (p2.X - p1.X)
                    < (p2.Y - p1.Y) * (p.X - p1.X))
                {
                    inside = !inside;
                }

                oldPoint = newPoint;
            }

            return inside;
        }
        
        static Vertex3D GetVertex(Vector2D key, Triangle3D triangle, Dictionary<Vector2D, Vertex3D> addedVertices, ref int vertexIdCounter)
        {
            // In order for the mesh creation to work correctly, there cannot be duplicate vertices (in on an edge shared by two triangles)
            // => each 2D position has a unique vertex.

            Vertex3D v;
            if (addedVertices.TryGetValue(key, out v)) return v;
            vertexIdCounter++;
            var ret = new Vertex3D(new Vector3D(key.X, key.Y, triangle.InterpolateZ(key)), vertexIdCounter);
            addedVertices.Add(key, ret);
            return ret;
        }

        void IntersectTriangulate(Triangle3D triangle, List<Triangle3D> result, Dictionary<Vector2D, Vertex3D> addedVertices, ref int vertexIdCounter)
        {
            var intersection = Intersect(triangle.ProjectedVertices);
            
            foreach (var poly in intersection)
            {
                var polyPoints = poly.Select(p => new Poly2Tri.PolygonPoint(p.X, p.Y)).ToList();
                var tri = new Poly2Tri.Polygon(polyPoints);
                Poly2Tri.P2T.Triangulate(tri);
                
                Vertex3D[] buffer = new Vertex3D[3];
                foreach (var t in tri.Triangles)
                {
                    double cX = 0.0, cY = 0.0;
                    for (int i = 0; i < 3; i++)
                    {
                        var p = t.Points[i];
                        cX += p.X;
                        cY += p.Y;
                        buffer[i] = GetVertex(new Vector2D(p.X, p.Y), triangle, addedVertices, ref vertexIdCounter);
                    }
                    // A triangle is inside if its center is inside.
                    if (IsPointInside(new Vector2D(cX / 3.0, cY / 3.0)))
                    {
                        result.Add(new Triangle3D(buffer) { Id = result.Count });
                    }
                }
            }
        }
        
        void Intersect(Triangle3D triangle, List<Triangle3D> result, Dictionary<Vector2D, Vertex3D> addedVertices, ref int vertexIdCounter)
        {
            var vertices = triangle.ProjectedVertices;
            double minX = double.MaxValue, maxX = double.MinValue;
            double minY = double.MaxValue, maxY = double.MinValue;
            
            // found the bounds
            for (int i = 0; i < 3; i++)
            {
                var v = vertices[i];
                minX = System.Math.Min(minX, v.X);
                minY = System.Math.Min(minY, v.Y);
                maxX = System.Math.Max(maxX, v.X);
                maxY = System.Math.Max(maxY, v.Y);
            }

            // find the segments that might intersect this triangle.

            var boolHasintersection = false;
            var segments = SegmentsLookup.GetSegmentsInRange(minX, maxX, minY, maxY);
            foreach (var e in triangle.ProjectedEdges)
            {
                foreach (var s in segments)
                {
                    var x = LineSegment2D.Intersection(s.Segment, e);
                    if (x.HasValue)
                    {
                        boolHasintersection = true;
                        break;
                    }

                    // polygon points can be in triangle only if the edge intersects it.
                    if (triangle.PointInTriangle(s.Segment.A))
                    {
                        boolHasintersection = true;
                        break;
                    }
                    if (triangle.PointInTriangle(s.Segment.B))
                    {
                        boolHasintersection = true;
                        break;
                    }
                    if (boolHasintersection) break;
                }
            }

            // add triangle points inside the polygon
            var allInside = true;
            var someInside = false;
            for (int i = 0; i < 3; i++)
            {
                var v = vertices[i];
                if (!IsPointInside(v)) allInside = false;
                else
                {
                    someInside = true;
                    addedVertices[v] = triangle.Vertices[i];
                }
            }

            // Is the triangle wholly inside?
            if (!boolHasintersection && allInside)
            {
                triangle.Id = result.Count;
                result.Add(triangle);
                return;
            }

            // the trigangle is outside
            if (!boolHasintersection && !someInside)
            {
                return;
            }

            // Is this some boundary case?
            //if (points.Count < 3) return;

            IntersectTriangulate(triangle, result, addedVertices, ref vertexIdCounter);

            //return;
            //if (isDefinitelyConvex || points.Count == 3) IntersectConvex(points, triangle, result, addedVertices, ref vertexIdCounter);
            //else IntersectTriangulate(points, triangle, result, addedVertices, ref vertexIdCounter);
        }

        internal void Clip(Triangle3D triangle, List<Triangle3D> result, Dictionary<Vector2D, Vertex3D> addedVertices, ref int vertexIdCounter)
        {
            // 1. Check if the triangle is outside the bounding box. If so, do not process further.
            // 2. Find common points:
            //    a) Check if some points of the triangle are inside the polygon
            //    a) Check if some points of the polygon are inside the triangle
            //    b) Check for polygon/triangle edge intersections
            // 3. Triangulate the points from step 2.
            // 4. Add triangles that are inside the polygon.
            //
            // Complexity O( # of vertices of polygon )
            
            if (!Boundary.TriangleIntersects(triangle)) return;
        
            Intersect(triangle, result, addedVertices, ref vertexIdCounter);
        }


        bool Intersects(Triangle3D triangle)
        {
            var vertices = triangle.ProjectedVertices;
            double minX = double.MaxValue, maxX = double.MinValue;
            double minY = double.MaxValue, maxY = double.MinValue;

            List<Vector2D> points = new List<Vector2D>();

            // found the bounds
            for (int i = 0; i < 3; i++)
            {
                var v = vertices[i];
                minX = System.Math.Min(minX, v.X);
                minY = System.Math.Min(minY, v.Y);
                maxX = System.Math.Max(maxX, v.X);
                maxY = System.Math.Max(maxY, v.Y);
                if (IsPointInside(v)) return true;
            }

            // find the segments that might intersect this triangle.
            var segments = SegmentsLookup.GetSegmentsInRange(minX, maxX, minY, maxY);
            foreach (var e in triangle.ProjectedEdges)
            {
                foreach (var s in segments)
                {
                    var x = LineSegment2D.Intersection(s.Segment, e);
                    if (x.HasValue) return true;
                    if (triangle.PointInTriangle(s.Segment.A)) return true;
                    if (triangle.PointInTriangle(s.Segment.B)) return true;
                }
            }

            return false;
        }

        internal bool Filter(Triangle3D triangle)
        {
            if (!Boundary.TriangleIntersects(triangle)) return false;

            return Intersects(triangle);
        }
    }
}
