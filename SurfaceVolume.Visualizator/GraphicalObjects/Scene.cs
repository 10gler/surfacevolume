﻿using Newtonsoft.Json;
using SurfaceVolume2.Export;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tao.OpenGl;

namespace Volume.Visualizator.GraphicalObjects
{
    class Scene
    {
        ExportableResult Result { get; set; }
        public RenderState State { get; set; } 

        double[] center;

        void Init()
        {
            center = new double[3];
            foreach (var v in Result.SurfaceA.Vertices.Concat(Result.SurfaceB.Vertices))
            {
                for (int i = 0; i < 3; i++) center[i] += v[i];
            }
            int len = Result.SurfaceA.Vertices.Length + Result.SurfaceB.Vertices.Length;
            center[0] *= 1.0 / len; center[1] *= 1.0 / len; center[2] *= 1.0 / len;
        }

        void RenderSurface(double[] color, ExportableSurface surface)
        {
            Gl.glPushMatrix();
            Gl.glPolygonMode(Gl.GL_FRONT_AND_BACK, Gl.GL_LINE);
            Gl.glColor3dv(color);

            Gl.glBegin(Gl.GL_TRIANGLES);
            foreach (var t in surface.Triangles)
            {
                Gl.glVertex3dv(surface.Vertices[t[0]]);
                Gl.glVertex3dv(surface.Vertices[t[1]]);
                Gl.glVertex3dv(surface.Vertices[t[2]]);
            }
            Gl.glEnd();
            Gl.glPopMatrix();
        }

        void RenderVolume(double[] color, int sign)
        {
            Gl.glPushMatrix();
            Gl.glPolygonMode(Gl.GL_FRONT_AND_BACK, Gl.GL_LINE);
            Gl.glColor3dv(color);

            Gl.glBegin(Gl.GL_TRIANGLES);
            foreach (var v in Result.Volumes)
            {
                if (Math.Sign(v.Volume) != sign) continue;
                
                var x = v.Vertices[0];
                Gl.glVertex3d(x[0], x[1], v.ZValuesA[0]);
                x = v.Vertices[1];
                Gl.glVertex3d(x[0], x[1], v.ZValuesA[1]);
                x = v.Vertices[2];
                Gl.glVertex3d(x[0], x[1], v.ZValuesA[2]);

                x = v.Vertices[0];
                Gl.glVertex3d(x[0], x[1], v.ZValuesB[0]);
                x = v.Vertices[1];
                Gl.glVertex3d(x[0], x[1], v.ZValuesB[1]);
                x = v.Vertices[2];
                Gl.glVertex3d(x[0], x[1], v.ZValuesB[2]);
            }
            Gl.glEnd();
            Gl.glPopMatrix();
        }

        void RenderClip()
        {
            Gl.glPushMatrix();
            Gl.glColor3dv(cColor);

            Gl.glBegin(Gl.GL_LINES);
            Gl.glLineWidth(8);
            foreach (var p in Result.ClipPolygons)
            {
                var vs = p.Vertices;
                for (int i = 0; i < vs.Length; i++)
                {
                    Gl.glVertex2dv(vs[i]);
                    Gl.glVertex2dv(vs[(i + 1) % vs.Length]);
                }
            }
            Gl.glLineWidth(1);
            Gl.glEnd();
            Gl.glPopMatrix();
        }

        readonly double[] aColor = new[] { 1.0, 0, 0 };
        readonly double[] bColor = new[] { 0.0, 0, 1.0 };

        readonly double[] acColor = new[] { 1.0, 1.0, 0 };
        readonly double[] bcColor = new[] { 0.0, 1.0, 1.0 };

        readonly double[] cColor = new[] { 1.0, 0.5, 0.3 };

        readonly double[] pColor = new[] { 1.0, 0.0, 1.0 };
        readonly double[] nColor = new[] { 1.0, 1.0, 1.0 };
        readonly double[] zColor = new[] { 0.5, 0.2, 0.8 };



        public void Render()
        {
            Gl.glTranslated(-center[0], -center[1], -center[2]);

            if (State.ShowSurfaceA) RenderSurface(aColor, Result.SurfaceA);
            if (State.ShowSurfaceB) RenderSurface(bColor, Result.SurfaceB);

            if (State.ShowClippedSurfaceA) RenderSurface(aColor, Result.ClippedSurfaceA);
            if (State.ShowClippedSurfaceB) RenderSurface(bColor, Result.ClippedSurfaceB);

            if (State.ShowClipPolygon) RenderClip();

            if (State.ShowPositiveVolume) RenderVolume(pColor, +1);
            if (State.ShowNegativeVolume) RenderVolume(nColor, -1);
            if (State.ShowZeroVolume) RenderVolume(zColor,  0);
        }

        public Scene(string filename)
        {
            Result = JsonConvert.DeserializeObject<ExportableResult>(File.ReadAllText(filename));
            State = new RenderState();
            Init();
        }
    }
}
