﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurfaceVolume2.Export
{
    public class VolumeTriangle
    {
        public double[][] Vertices { get; set; }
        public double[] ZValuesA { get; set; }
        public double[] ZValuesB { get; set; }
        public double Volume { get; set; }
    }

    public class ExportablePolygon
    {
        public double[][] Vertices { get; set; }
    }

    public class ExportableSurface
    {
        public double[][] Vertices { get; set; }
        public int[][] Triangles { get; set; }
    }

    public class ExportableResult
    {
        public ExportableSurface SurfaceA { get; set; }
        public ExportableSurface SurfaceB { get; set; }

        public ExportableSurface ClippedSurfaceA { get; set; }
        public ExportableSurface ClippedSurfaceB { get; set; }

        public ExportablePolygon[] ClipPolygons { get; set; }

        public VolumeTriangle[] Volumes { get; set; }

        public double PositiveVolume { get; set; }
        public double NegativeVolume { get; set; }

        public double ExpectedPositiveVolume { get; set; }
        public double ExpectedNegativeVolume { get; set; }
    }

}
