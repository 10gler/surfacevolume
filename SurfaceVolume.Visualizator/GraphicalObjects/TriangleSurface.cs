﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tao.OpenGl;
using Volume.Visualizator.Helpers;

namespace Volume.Visualizator.GraphicalObjects
{
    public class TriangleSurface
    {
        private IList<Triangle> _triangles;
        public Vector3d Color { get; set; }

        public double MinX { get; private set; }
        public double MaxX { get; private set; }
        public double MinY { get; private set; }
        public double MaxY { get; private set; }
        public double MinZ { get; private set; }
        public double MaxZ { get; private set; }

        public TriangleSurface(IList<Triangle> triangles, Vector3d color = null)
        {            
            _triangles = triangles;
            Color = color ?? new Vector3d(1, 1, 1);
            MinX = _triangles.Min(v => v.MinX);
            MinY = _triangles.Min(v => v.MinY);
            MinZ = _triangles.Min(v => v.MinZ);
            MaxX = _triangles.Max(v => v.MaxX);
            MaxY = _triangles.Max(v => v.MaxY);
            MaxZ = _triangles.Max(v => v.MaxZ);
        }

        public void Draw()
        {
            Gl.glPushMatrix();
            Gl.glPolygonMode(Gl.GL_FRONT_AND_BACK, Gl.GL_LINE);
            Gl.glColor3dv(Color);

            Gl.glBegin(Gl.GL_TRIANGLES);
            foreach (var t in _triangles)
            {
                Gl.glVertex3d(t.V1.X, t.V1.Y, t.V1.Z);
                Gl.glVertex3d(t.V2.X, t.V2.Y, t.V2.Z);
                Gl.glVertex3d(t.V3.X, t.V3.Y, t.V3.Z);
            }
            Gl.glEnd();
            Gl.glPopMatrix();
        }
    }
}
