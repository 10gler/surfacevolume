﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volume.Visualizator.GraphicalObjects
{
    class RenderState
    {
        public bool ShowSurfaceA { get; set; }
        public bool ShowSurfaceB { get; set; }

        public bool ShowClippedSurfaceA { get; set; }
        public bool ShowClippedSurfaceB { get; set; }

        public bool ShowClipPolygon { get; set; }

        public bool ShowPositiveVolume { get; set; }
        public bool ShowNegativeVolume { get; set; }
        public bool ShowZeroVolume { get; set; }

        public bool ShowClipPolygons { get; set; }
    }
}
