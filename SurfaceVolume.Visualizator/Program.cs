﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Tao.FreeGlut;
using Tao.OpenGl;
using Volume.Visualizator.Helpers;
using Volume.Visualizator.GraphicalObjects;
using System.Windows;
using System.Windows.Controls;

namespace Volume.Visualizator
{
    class Program
    {
        static TriangleSurface GetSurfaceFromXml(string xmlPath)
        {
            List<Triangle> triangles = XElement.Load(xmlPath).Elements("t")
                .Select(t => new Triangle(t.Elements("v")
                    .Select(v =>
                    {
                        return new Vector3d(double.Parse(v.Attribute("x").Value, CultureInfo.InvariantCulture), double.Parse(v.Attribute("y").Value, CultureInfo.InvariantCulture),
                                            double.Parse(v.Attribute("z").Value, CultureInfo.InvariantCulture));
                    }).ToList())).ToList();
            return new TriangleSurface(triangles);      
        }

        static void AddStateControl(StackPanel controls, string name, Action<bool> onChange)
        {
            var box = new CheckBox { Content = name, Margin = new Thickness(6, 4, 6, 2) };
            box.Checked += (s, a) => onChange(((CheckBox)s).IsChecked.Value);
            box.Unchecked += (s, a) => onChange(((CheckBox)s).IsChecked.Value);
            controls.Children.Add(box);
        }

        [STAThread]
        static void Main(string[] args)
        {
            var scene = new Scene(@".\..\..\ExportResults\2-surfaces-hexagon.json");
            //var scene = new Scene(@"I:\Projects\Misc\surfacevolume\tests\plane-sin.json");
            
            

            var stateWindow = new Window { Title = "State", Width = 200, Height = 300 };
            var controls = new StackPanel();
            AddStateControl(controls, "Show Surface A", b => scene.State.ShowSurfaceA = b);
            AddStateControl(controls, "Show Surface B", b => scene.State.ShowSurfaceB = b);
            AddStateControl(controls, "Show Clipped Surface A", b => scene.State.ShowClippedSurfaceA = b);
            AddStateControl(controls, "Show Clipped Surface B", b => scene.State.ShowClippedSurfaceB = b);
            AddStateControl(controls, "Show Clip Polygons", b => scene.State.ShowClipPolygon = b);
            AddStateControl(controls, "Show Positive", b => scene.State.ShowPositiveVolume = b);
            AddStateControl(controls, "Show Negative", b => scene.State.ShowNegativeVolume = b);
            AddStateControl(controls, "Show Zero", b => scene.State.ShowZeroVolume = b);
            var showAll = new Button { Content = "Show All", Margin = new Thickness(6, 4, 6, 2) };
            showAll.Click += (_, __) =>
            {
                foreach (var c in controls.Children.OfType<CheckBox>()) c.IsChecked = true;
            };
            var hideAll = new Button { Content = "Hide All", Margin = new Thickness(6, 4, 6, 2) };
            hideAll.Click += (_, __) =>
            {
                foreach (var c in controls.Children.OfType<CheckBox>()) c.IsChecked = false;
            };
            controls.Children.Add(showAll);
            controls.Children.Add(hideAll);
            stateWindow.Content = controls;
            stateWindow.Show();

            var sc = new ConsoleGLScene(() => 
            {
                scene.Render();    
            }, new Vector3d(0, 0, 1500));            
        }
    }
}
