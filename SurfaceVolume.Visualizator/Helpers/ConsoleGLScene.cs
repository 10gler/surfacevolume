﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tao.FreeGlut;
using Tao.OpenGl;

namespace Volume.Visualizator.Helpers
{
    public class ConsoleGLScene
    {
        private int mouseButton;
        private int moueseState;
        private int pitch;
        private int roll;
        private const double dtr = Math.PI / 180.0;
        private const double motionSpeed = 100;
        private const double rotationSpeed = 0.3;

        private Camera _camera;
        private Action _render;

        public Vector3d LightPosition { get; set; }

        #region OpenGL setup
        private void Init()
        {
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_DOUBLE);
            Glut.glutInitWindowSize(800, 600);
            Glut.glutCreateWindow("Console scene window");
            //Glut.glutFullScreen();

            Gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Glu.gluPerspective(60, 1.0, 1.0, 100000.0);
            //const double delta = 6000;
            //Gl.glOrtho(-delta, delta, -delta, delta, -delta, delta);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            //Gl.glEnable(Gl.GL_LIGHT0);
            //Gl.glEnable(Gl.GL_LIGHTING);

            //register keyboard and mouse events
            Glut.glutMouseFunc((button, state, x, y) => 
            {
                if ( mouseButton == Glut.GLUT_LEFT_BUTTON && state == Glut.GLUT_DOWN )
	            {
		            pitch = y;
		            roll = x;
	            }
	            mouseButton = button;
	            moueseState = state;
            });
            Glut.glutMotionFunc((x, y) =>
            {
                if (mouseButton == Glut.GLUT_LEFT_BUTTON && moueseState == Glut.GLUT_DOWN)
                {
                    //_camera.LookSide(-(x - roll) * dtr * rotationSpeed);
                    //_camera.LookUpDown(-(y - pitch) * dtr * rotationSpeed);

                    _camera.Look(-(x - roll) * dtr * rotationSpeed, -(y - pitch) * dtr * rotationSpeed);

                    //_camera.Pitch = -(y - pitch) * dtr * rotationSpeed;
                    //_camera.Roll = -(x - roll) * dtr * rotationSpeed;

                }
                pitch = y;
                roll = x;
            });

            Glut.glutKeyboardFunc((key, y, z) => 
            {
                int mod = Glut.glutGetModifiers();
                switch (key)
                {
                    //exit program
                    case 27:
                        Environment.Exit(0);
                        break;
                    case (byte)'q':
                        _camera.MoveUp(motionSpeed);
                        break;
                    case (byte)'e':
                        _camera.MoveUp(-motionSpeed);
                        break;
                    case (byte)'a':
                        _camera.MoveSide(-motionSpeed);
                        break;
                    case (byte)'d':
                        _camera.MoveSide(motionSpeed);
                        break;
                    case (byte)'w':
                        _camera.MoveFov(motionSpeed);
                        break;
                    case (byte)'s':
                        _camera.MoveFov(-motionSpeed);
                        break;
                    //////up
                    ////case (byte)'u':
                    ////    _camera.MoveUpward(motionSpeed);
                    ////    break;
                    //////down
                    ////case (byte)'d':
                    ////    _camera.MoveUpward(-motionSpeed);
                    ////    break;
                }
            });
            Glut.glutSpecialFunc((key, x, y) =>
            {
                switch (key)
                {
                    case (byte)'a':
                        _camera.MoveSide(-motionSpeed);
                        break;
                    case (byte)'d':
                        _camera.MoveSide(motionSpeed);
                        break;
                    case (byte)'w':
                        _camera.MoveFov(motionSpeed);
                        break;
                    case (byte)'s':
                        _camera.MoveFov(-motionSpeed);
                        break;
                }
            });

            Glut.glutIdleFunc(Render);
            Glut.glutDisplayFunc(Render);
            Glut.glutMainLoop();
        }

        private void Light()
        {
            float[] AmbientLight = { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] DiffuseLight = { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] SpecularLight = { 1.0f, 1.0f, 1.0f, 1.0f };
            float[] SpecularMatrial = { 1.0f, 1.0f, 1.0f, 1.0f };
            float[] PositionLight = { (float)LightPosition.X, (float)LightPosition.Y, (float)LightPosition.Z, 1.0f };

            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_AMBIENT, AmbientLight);
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_DIFFUSE, DiffuseLight);
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_SPECULAR, SpecularLight);
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, PositionLight);

            Gl.glEnable(Gl.GL_COLOR_MATERIAL);
            Gl.glColorMaterial(Gl.GL_FRONT_AND_BACK, Gl.GL_AMBIENT_AND_DIFFUSE);
            Gl.glMaterialfv(Gl.GL_FRONT_AND_BACK, Gl.GL_SPECULAR, SpecularMatrial);
            Gl.glMaterialf(Gl.GL_FRONT_AND_BACK, Gl.GL_SHININESS, 50.0f);

            //draw light
            //Gl.glPushMatrix();
            //Gl.glColor3f(1.0f, 1.0f, 1.0f);
            //Gl.glTranslatef(PositionLight[0], PositionLight[1], PositionLight[2]);
            //Gl.gluSphere(q, 0.5, 20, 20);
            //Gl.glPopMatrix();
        }
        #endregion

        private void Render()
        {
            if (_render != null)
            {
                Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);

                Gl.glPushMatrix();
                _camera.Draw();
                Light();
                _render();
                Gl.glPopMatrix();

                Gl.glFlush();
                Glut.glutSwapBuffers();
            }
        }

        public ConsoleGLScene(Action render, Vector3d initialCameraPos)
        {
            LightPosition = new Vector3d(0, 1, 10);
            _camera = new Camera();

            //_camera.Position = new Vector3d(0, 1, 10);
            _camera.Position = initialCameraPos;
            _render = render;

            Init();
        }
    }
}
