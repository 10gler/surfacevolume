﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volume.Visualizator.Helpers
{
    public class Vector3d : IEnumerable
    {
        private double[] xyz = new double[3];

        public double X { get { return this.xyz[0]; } set { this.xyz[0] = value; } }
        public double Y { get { return this.xyz[1]; } set { this.xyz[1] = value; } }
        public double Z { get { return this.xyz[2]; } set { this.xyz[2] = value; } }

        public Vector3d(Vector3d o)
        {
            this.xyz[0] = this.X = o.X;
            this.xyz[1] = this.Y = o.Y;
            this.xyz[2] = this.Z = o.Z;
        }

        public Vector3d()
        {

        }

        public double this[int i]
        {
            get
            {
                return this.xyz[i];
            }
            set
            {
                this.xyz[i] = value;
            }
        }

        public Vector3d(double[] xyz)
        {
            xyz.CopyTo(this.xyz, 0);
            this.X = this.xyz[0];
            this.Y = this.xyz[1];
            this.Z = this.xyz[2];
        }

        public Vector3d(double x, double y, double z)
        {
            this.xyz[0] = this.X = x;
            this.xyz[1] = this.Y = y;
            this.xyz[2] = this.Z = z;
        }

        public double GetLength()
        {
            return Math.Sqrt(Math.Pow(this.X, 2) + Math.Pow(this.Y, 2) + Math.Pow(this.Z, 2));
        }

        public Vector3d Normalized()
        {
            double len = this.GetLength();
            if (len != 0)
                return new Vector3d(this.X / len, this.Y / len, this.Z / len);
            else return new Vector3d();
        }

        public override string ToString()
        {
            return string.Format("[{0:F2}, {1:F2}, {2:F2}]", this.X, this.Y, this.Z);
        }

        //Operators
        public static Vector3d operator +(Vector3d a, Vector3d b)
        {
            return new Vector3d(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Vector3d operator -(Vector3d a, Vector3d b)
        {
            return new Vector3d(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }
        /// <summary>
        /// Calculate scalar product
        /// </summary>
        /// <param name="a">First vector</param>
        /// <param name="b">Second vector</param>
        /// <returns>Returns scalar product</returns>
        public static double operator *(Vector3d a, Vector3d b)
        {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }

        public static Vector3d operator *(Vector3d a, double b)
        {
            return new Vector3d(a.X * b, a.Y * b, a.Z * b);
        }

        public Vector3d CrossProduct(Vector3d vec)
        {
            return new Vector3d(Y * vec.Z - Z * vec.Y, Z * vec.X - X * vec.Z, X * vec.Y - Y * vec.X);
        }

        public static implicit operator double[](Vector3d vec)
        {
            return new double[] { vec.X, vec.Y, vec.Z };
        }

        public double Angle(Vector3d other)
        {
            //u*v = |u|*|v|*cos(u,v)
            return Math.Acos((this * other) / (this.GetLength() * other.GetLength()));
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return (this.xyz as IEnumerable).GetEnumerator();
        }

        #endregion
    }

}
