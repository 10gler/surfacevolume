﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tao.FreeGlut;
using Tao.OpenGl;
using WebChemistry.Framework.Math;

namespace Volume.Visualizator.Helpers
{
    public class Camera
    {
        private Vector3d _forward;
        private Vector3d _right;
        private Vector3d _target;
        private Vector3d _up;
        private double _pitch;
        private double _roll;
        private double _yaw;

        public Camera()
        {
            _forward = new Vector3d(0, 0, -1);
            _right = new Vector3d(1, 0, 0);
            _up = new Vector3d(0, 1, 0);
            //the target where to look at
            _target = new Vector3d(0, 0, 0);
            //camera position
            Position = new Vector3d(0, 0, 0);
            //camera angles
            _pitch = _roll = _yaw = 0;
        }

        private void SetZeroRotation()
        {
            _pitch = _roll = _yaw = 0;
        }

        public void SetTarget(Vector3d target)
        {
            //setZeroRotation();
            if ((Position - target).GetLength() == 0) //position and target can be at the same point
                return;
            this._target = target;
            _forward = (target - this.Position);
            _forward.Normalized();
        }

        public Vector3d Position { get; set; }
        public double Pitch 
        {
            get
            {
                return _pitch;
            }
            set
            {
                _pitch += value;
                _forward = (_forward * Math.Cos(value) + _up * Math.Sin(value));
                _forward.Normalized();
            }
        }
        public double Roll 
        {
            get
            {
                return _roll;
            }
            set 
            {
                _roll += value;
                _forward = (_forward * Math.Cos(value) - _right * Math.Sin(value));
                _forward.Normalized();
                _right = _forward.CrossProduct(_up);
            } 
        }
        public double Yaw 
        {
            get
            {
                return _yaw;
            }
            set
            {
                _yaw += value;
                _right = (_right* Math.Cos(value) + _up * Math.Sin(value));
                _right.Normalized();
                _up = _right.CrossProduct(_forward);
            }
        }
        
        public void LookUpDown(double angle)
        {
            var axis = _forward.CrossProduct(_up);
            var q = Quaternion.Rotation(axis, angle);
            _forward = q.Transform(_forward);
            _up = q.Transform(_up);
        }

        public void LookSide(double angle)
        {
            var axis = new Vector3d(0, 0, _up.Z > 0 ? 1 : -1);
            var q = Quaternion.Rotation(axis, angle);
            _forward = q.Transform(_forward);
            _up = q.Transform(_up);
        }

        public void Look(double sideAngle, double upAngle)
        {
            var q = Quaternion.Rotation(_forward.CrossProduct(_up), upAngle) 
                * Quaternion.Rotation(new Vector3d(0, 0, _up.Z > 0 ? 1 : -1), sideAngle);
            _forward = q.Transform(_forward);
            _up = q.Transform(_up);
        }

        public void Move(Vector3d where)
        {
            Position = Position + where;
        }

        public void MoveForward(double distance)
        { 
            Vector3d forwardTmp = new Vector3d(_forward.X, 0, _forward.Z);
	        Position = Position + forwardTmp*distance;
        }

        public void MoveLeftRight(double distance)
        {
            Position = Position + _right * distance;
        }

        public void MoveUpward(double distance)
        { 
        	Vector3d upTmp = new Vector3d(0, _up.Y, 0);
	        Position = Position + upTmp*distance;
        }

        public void MoveUp(double distance)
        {
            Position = Position + _up * distance;
        }

        public void MoveFov(double distance)
        {
            Position = Position + _forward * distance;
        }

        public void MoveSide(double distance)
        {
            var dir = _forward.CrossProduct(_up);
            Position = Position + dir * distance;
        }

        public void Draw()
        {
            Vector3d centerOfView = Position + _forward;

            Glu.gluLookAt(Position.X, Position.Y, Position.Z,
                        centerOfView.X, centerOfView.Y, centerOfView.Z,
                        _up.X, _up.Y, _up.Z);
        }
    }
}
