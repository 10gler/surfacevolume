﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volume.Visualizator.Helpers
{
    public class Triangle
    {
        private IEnumerable<Vector3d> _vertices;

        public Vector3d V1 { get; set; }
        public Vector3d V2 { get; set; }
        public Vector3d V3 { get; set; }

        public double MinX { get; private set; }
        public double MaxX { get; private set; }
        public double MinY { get; private set; }
        public double MaxY { get; private set; }
        public double MinZ { get; private set; }
        public double MaxZ { get; private set; }
        
        public Triangle(Vector3d v1, Vector3d v2, Vector3d v3)
        {
            V1 = v1;
            V2 = v2;
            V3 = v3;

            _vertices = new List<Vector3d>() { v1, v2, v3 };

            MinX = _vertices.Min(v => v.X);
            MinY = _vertices.Min(v => v.Y);
            MinZ = _vertices.Min(v => v.Z);
            MaxX = _vertices.Max(v => v.X);
            MaxY = _vertices.Max(v => v.Y);
            MaxZ = _vertices.Max(v => v.Z);
        }

        public Triangle(IList<Vector3d> vertices) : this(vertices[0], vertices[1], vertices[2])
        {
        }
    }
}
